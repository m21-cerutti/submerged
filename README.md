# Submerged

Submerged is a two player digital escape room in situ where players interact with a mermaid AI through head tracking and object recognition mechanics using Unity, an Azure Kinect and NFC technology.

## How to use it
---

You will need 3 three monitors at least, a Kinect Azure and a Nucleo Card F411RE with Sheild NFC 03A1.
For the arduino card, you will need to televerse two times the [./nfc/nfc_rw/nfc_rw.ino](./nfc/nfc_rw/nfc_rw.ino) file before running the build.

You can configure the starting parameters in the Submerged_Data\StreamingAssets folder with some Json files for configuring the arduino card, monitors and the kinect.

You will have when started the MJ interface below, that permit to debug if all hardware is Ok, the AI flow and different usefull information. Report on the AI section for what is it possible to do.

![](./doc/mj_interface.jpg)

### Test without Kinect Azure and Nucleo Card hardware

You can test the software without this two materials by disabling in Unity the Hardware section in the Game Scene. Like that, only manual trigerring can be done on events with object detection and no tracking will occure.

## Hierarchy
---

- Folders (Assets etc) from Unity
- ci for CI/CD scripts
- nfc for Arduino files
- FMOD for Fmod project


## Unity Developement
---

### Features

- Mermaid AI (Drive system)
- Google sheet synchronisation
- Submerged virtual environment
- Multiple monitors
- Surround audio 5.1 (fmod)
- Arduino NFC communication
- Kinect detection

### Dependancies

- Unity 2021.3.2f1 HDRP
- Fmod 2.02.04
- Hierarchy 2
- ClapClapEvent
- Kinect azure SDK 1.4.1
https://github.com/microsoft/Azure-Kinect-Sensor-SDK/blob/develop/docs/usage.md
- Driver UC (For sound Card)
https://www.rme-audio.de/downloads.html



### Register new objects

If you know the card UID, you cant put it directly in "Assets/StreamingAssets/arduino.json".
Otherwise open the build or Unity and switch to REGISTER_FILE mode (in Unity on ArduinoSerial script). When new object will be presented, it will register the UID in the file when stoping.
Put the key name of the object (list in PhysicObject enum) inside the json field "m_key".
And now when the object will be presented and removed in READ mode, it will trigger the event associated to the object.

### AI

![](./doc/ia_table.jpg)

```plantuml
@startuml

() GoogleSheet 
() Backup 

package CSV
{
class CSVDownloader
class CSVConverter
}

package AI
{
class Condition
{
    bool evaluate(GameController)
}
class Action
{
    void exec(GameController)
}
class AiBehavior
{
    int ID
    string name
}
}

class GameController
{
    //Acess to states
}

GoogleSheet ..> CSVDownloader
Backup <-- CSVDownloader
CSVConverter <- Backup : if failed
CSVDownloader --> CSVConverter : if sucess

CSVConverter --> AiBehavior
AiBehavior --* Action : 0..nb_action_type
AiBehavior --* Condition: 0..nb_condition_type

BehaviorContainer --* AiBehavior

ScriptableObject <|- BehaviorContainer 

GameController --> BehaviorContainer : call with own ref


@enduml
```


## Arduino developement
---

### Installation

1. Arduino IDE

2. Install Libraries
    - STM32duino X-NUCLEO-NFC03A1
    - STM32duino NFC-RFAL
    - STM32duino ST25R95

3. Install card STM32 Boards

(https://www.instructables.com/Quick-Start-to-STM-Nucleo-on-Arduino-IDE/)
(https://github.com/stm32duino/wiki/wiki/Getting-Started)

4. Set the configuration like this with baud 115200

![](./nfc/config.png)

### Known issues

You need to televerse two times before you can use it with the software.
If Serial in Arduino is not opening, spam serial in update loop with blink to unlocked it.

### Hardware

- Carte Nucleo F411RE 
- X-Nucleo NFC03A1

## Kinect development
---

See https://gitlab.com/m21-cerutti/samplekinect-multibodytracking



## CI/CD
---

See [./ci/README.md](./ci/README.md)
