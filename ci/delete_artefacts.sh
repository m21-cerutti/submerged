#!/usr/bin/env bash

set -e

# How many to delete from the oldest.
if [ -z $PER_PAGE ]
then
    PER_PAGE=1
fi

# From the oldest by default.
if [ -z $SORT ]
then
    SORT="asc"
fi

for PIPELINE in $(curl --header "PRIVATE-TOKEN: $OAUTH2_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines?per_page=$PER_PAGE&sort=$SORT" | jq '.[].id') ; do
    echo "Deleting pipeline $PIPELINE"
    curl --header "PRIVATE-TOKEN: $OAUTH2_TOKEN" --request "DELETE" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$PIPELINE"
done

