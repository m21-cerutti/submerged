#include <string>
#include "nfc_utils.h"
#include "rfal_nfc.h"
#include "rfal_rfst25r95.h"

#define SPI_MOSI D11
#define SPI_MISO D12
#define SPI_SCK D13
#define CS_PIN D10
#define LED_A_PIN D7
#define LED_B_PIN D6
#define LED_F_PIN D5
#define LED_V_PIN D4
#define IRQ_IN_PIN D8
#define IRQ_OUT_PIN D2
#define INTERFACE_PIN D9

// States
#define NOTINIT 0
#define START_DISCOVERY 1
#define DISCOVERY 2
#define ACTION_COMPLETE 3


#define DELAY_TAG_REMOVED 200

/*
 ******************************************************************************
*/
#define DEBUG 1

#ifdef DEBUG
  #define LOG(x)  Serial.print("LOG: ");Serial.println(x)
#else
  #define LOG(x)
#endif

#define ERROR(x) Serial.print("ERROR: ");Serial.println(x)

/*
 ******************************************************************************
*/

/* SPI, Component and NFC */
SPIClass dev_spi(SPI_MOSI, SPI_MISO, SPI_SCK);
RfalRfST25R95Class rfst25r95(&dev_spi, CS_PIN, IRQ_IN_PIN, IRQ_OUT_PIN, INTERFACE_PIN);
RfalNfcClass rfal_nfc(&rfst25r95);

/* P2P communication data */
static rfalNfcDiscoverParam discParam;
static uint8_t NFCID3[] = { 0x01, 0xFE, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A };
static uint8_t GB[] = { 0x46, 0x66, 0x6d, 0x01, 0x01, 0x11, 0x02, 0x02, 0x07, 0x80, 0x03, 0x02, 0x00, 0x03, 0x04, 0x01, 0x32, 0x07, 0x01, 0x03 };

/* States */
static uint8_t state = NOTINIT;
int PushButtonState = 0;
String UID;

/*
******************************************************************************
*/

static void usage();
static void ledsOn();
static void ledsOff();
static char *hex2Str(unsigned char *data, size_t dataLen);
static void checkUserButton();
static void waitTagRemoved(rfalNfcDevice *nfcDevice);
/*
******************************************************************************
*/

void usage() {
  LOG("Print the UID of the card detected. Reset after the card is removed.");
}

/*
******************************************************************************
*/

void setup() {
  Serial.begin(115200);
  dev_spi.begin();

  pinMode(LED_A_PIN, OUTPUT);
  pinMode(LED_B_PIN, OUTPUT);
  pinMode(LED_F_PIN, OUTPUT);
  pinMode(LED_V_PIN, OUTPUT);
  pinMode(USER_BTN, INPUT);

  

  // Check what is the Push Button State when the button is not pressed. It can change across families
  PushButtonState = (digitalRead(USER_BTN)) ? 0 : 1;

  LOG("Init Card...");

  usage();

  // Init NFC
  if (rfal_nfc.rfalNfcInitialize() == ERR_NONE) {
    discParam.compMode = RFAL_COMPLIANCE_MODE_NFC;
    discParam.devLimit = 1U;
    discParam.nfcfBR = RFAL_BR_212;
    discParam.ap2pBR = RFAL_BR_424;

    ST_MEMCPY(&discParam.nfcid3, NFCID3, sizeof(NFCID3));
    ST_MEMCPY(&discParam.GB, GB, sizeof(GB));
    discParam.GBLen = sizeof(GB);

    discParam.notifyCb = nullptr;
    discParam.totalDuration = 1000U;
    discParam.wakeupEnabled = false;
    discParam.wakeupConfigDefault = true;
    discParam.techs2Find = (RFAL_NFC_POLL_TECH_A | RFAL_NFC_POLL_TECH_B | RFAL_NFC_POLL_TECH_F | RFAL_NFC_POLL_TECH_V | RFAL_NFC_POLL_TECH_ST25TB);

    state = START_DISCOVERY;
    LOG("Init Done.");
  }
  else
  {
    ERROR("Init incomplete");
  }
}

void loop() {
  static rfalNfcDevice *nfcDevice;

  rfal_nfc.rfalNfcWorker();

  checkUserButton();

  // Test connection
  if (Serial.available() > 0) {
      char code = Serial.read();
      Serial.println(code);
  }

  switch (state) {
    case START_DISCOVERY:
      ledsOff();
      rfal_nfc.rfalNfcDeactivate(false);
      rfal_nfc.rfalNfcDiscover(&discParam);
      state = DISCOVERY;
      break;

    case DISCOVERY:
      if (rfalNfcIsDevActivated(rfal_nfc.rfalNfcGetState())) {
        rfal_nfc.rfalNfcGetActiveDevice(&nfcDevice);
        delay(50);
        UID = String(hex2Str(nfcDevice->nfcid, nfcDevice->nfcidLen));
        // Print for Unity
        Serial.println("D:" +UID);
        ledsOn();
        state = ACTION_COMPLETE;
      }
      break;

    case ACTION_COMPLETE:
      //Loop until tag is removed from the field 
      if (rfalNfcIsDevActivated(rfal_nfc.rfalNfcGetState())) {
        rfal_nfc.rfalNfcGetActiveDevice(&nfcDevice);
        delay(50);
        UID = String(hex2Str(nfcDevice->nfcid, nfcDevice->nfcidLen));
        waitTagRemoved(nfcDevice);
        Serial.println("R:" + UID);
        state = START_DISCOVERY;
        rfal_nfc.rfalNfcDeactivate(false);
        delay(500);
      }
      state = START_DISCOVERY;
      break;

    case NOTINIT:
    default:
      break;
  }

}

/*****************************************************************************/

void waitTagRemoved(rfalNfcDevice *nfcDevice)
{
  rfalNfcaSensRes sensRes;
  rfalNfcaSelRes selRes;

  rfalNfcbSensbRes sensbRes;
  uint8_t sensbResLen;

  uint8_t devCnt = 0;
  rfalFeliCaPollRes cardList[1];
  uint8_t collisions = 0U;
  rfalNfcfSensfRes *sensfRes;

  rfalNfcvInventoryRes invRes;
  uint16_t rcvdLen;
  switch (nfcDevice->type) {
    case RFAL_NFC_LISTEN_TYPE_NFCA:
      rfal_nfc.rfalNfcaPollerInitialize();
      switch (nfcDevice->dev.nfca.type) {
        case RFAL_NFCA_T1T:
          rfal_nfc.rfalNfcaPollerSleep();
          break;

        case RFAL_NFCA_T4T:
          rfal_nfc.rfalIsoDepDeselect();
          break;

        case RFAL_NFCA_T4T_NFCDEP:
        case RFAL_NFCA_NFCDEP:
        default:
          rfal_nfc.rfalNfcaPollerSleep();
          break;
      }
      
      while (rfal_nfc.rfalNfcaPollerCheckPresence(RFAL_14443A_SHORTFRAME_CMD_WUPA, &sensRes) == ERR_NONE) {
        if (((nfcDevice->dev.nfca.type == RFAL_NFCA_T1T) && (!rfalNfcaIsSensResT1T(&sensRes))) || ((nfcDevice->dev.nfca.type != RFAL_NFCA_T1T) && (rfal_nfc.rfalNfcaPollerSelect(nfcDevice->dev.nfca.nfcId1, nfcDevice->dev.nfca.nfcId1Len, &selRes) != ERR_NONE))) {
          break;
        }
        rfal_nfc.rfalNfcaPollerSleep();
        delay(DELAY_TAG_REMOVED);
      }
      break;

    case RFAL_NFC_LISTEN_TYPE_NFCB:
      if (rfalNfcbIsIsoDepSupported(&nfcDevice->dev.nfcb)) {
        rfal_nfc.rfalIsoDepDeselect();
      } else {
        rfal_nfc.rfalNfcbPollerSleep(nfcDevice->dev.nfcb.sensbRes.nfcid0);
      }
      rfal_nfc.rfalNfcbPollerInitialize();
      while (rfal_nfc.rfalNfcbPollerCheckPresence(RFAL_NFCB_SENS_CMD_ALLB_REQ, RFAL_NFCB_SLOT_NUM_1, &sensbRes, &sensbResLen) == ERR_NONE) {
        if (ST_BYTECMP(sensbRes.nfcid0, nfcDevice->dev.nfcb.sensbRes.nfcid0, RFAL_NFCB_NFCID0_LEN) != 0) {
          break;
        }
        rfal_nfc.rfalNfcbPollerSleep(nfcDevice->dev.nfcb.sensbRes.nfcid0);
        delay(DELAY_TAG_REMOVED);
      }
      break;

    case RFAL_NFC_LISTEN_TYPE_NFCF:
      devCnt = 1;
      rfal_nfc.rfalNfcfPollerInitialize(RFAL_BR_212);
      while (rfal_nfc.rfalNfcfPollerPoll(RFAL_FELICA_1_SLOT, RFAL_NFCF_SYSTEMCODE, RFAL_FELICA_POLL_RC_NO_REQUEST, cardList, &devCnt, &collisions) == ERR_NONE) {
        //Skip the length field byte
        sensfRes = (rfalNfcfSensfRes *)&((uint8_t *)cardList)[1];
        if (ST_BYTECMP(sensfRes->NFCID2, nfcDevice->dev.nfcf.sensfRes.NFCID2, RFAL_NFCF_NFCID2_LEN) != 0) {
          break;
        }
        delay(DELAY_TAG_REMOVED);
      }
      break;
    case RFAL_NFC_LISTEN_TYPE_NFCV:
      rfal_nfc.rfalNfcvPollerInitialize();
      while (rfal_nfc.rfalNfcvPollerInventory(RFAL_NFCV_NUM_SLOTS_1, RFAL_NFCV_UID_LEN * 8U, nfcDevice->dev.nfcv.InvRes.UID, &invRes, &rcvdLen) == ERR_NONE) {
        delay(DELAY_TAG_REMOVED);
      }
      break;

    case RFAL_NFC_LISTEN_TYPE_ST25TB:
    case RFAL_NFC_LISTEN_TYPE_AP2P:
    default:
      break;
  }
}

#define MAX_HEX_STR 4
#define MAX_HEX_STR_LENGTH 128
char hexStr[MAX_HEX_STR][MAX_HEX_STR_LENGTH];
uint8_t hexStrIdx = 0;
char *hex2Str(unsigned char *data, size_t dataLen) {
  unsigned char *pin = data;
  const char *hex = "0123456789ABCDEF";
  char *pout = hexStr[hexStrIdx];
  uint8_t i = 0;
  uint8_t idx = hexStrIdx;
  size_t len;

  if (dataLen == 0) {
    pout[0] = 0;
  } else {
    /* Trim data that doesn't fit in buffer */
    len = MIN(dataLen, (MAX_HEX_STR_LENGTH / 2));

    for (; i < (len - 1); ++i) {
      *pout++ = hex[(*pin >> 4) & 0xF];
      *pout++ = hex[(*pin++) & 0xF];
    }
    *pout++ = hex[(*pin >> 4) & 0xF];
    *pout++ = hex[(*pin) & 0xF];
    *pout = 0;
  }

  hexStrIdx++;
  hexStrIdx %= MAX_HEX_STR;

  return hexStr[idx];
}

/*****************************************************************************/

void checkUserButton() {
  if (digitalRead(USER_BTN) == PushButtonState) {
    
    ledsOn();
    switch (state) {
      case START_DISCOVERY:
        LOG("START_DISCOVERY");
        break;
      case DISCOVERY:
        LOG("DISCOVERY");
        break;
      case ACTION_COMPLETE:
        LOG("ACTION_COMPLETE");
        break;
      case NOTINIT:
        LOG("NOTINIT");
        break;
      default:
        ERROR("Not a valid state");
        break;
    }

    //Debounce button 
    delay(50);
    while ((digitalRead(USER_BTN) == PushButtonState));
    delay(50);

    ledsOff();
  }
}

static void ledsOn(void) {
  digitalWrite(LED_A_PIN, HIGH);
  digitalWrite(LED_B_PIN, HIGH);
  digitalWrite(LED_F_PIN, HIGH);
  digitalWrite(LED_V_PIN, HIGH);
}

static void ledsOff(void) {
  digitalWrite(LED_A_PIN, HIGH);
  digitalWrite(LED_B_PIN, LOW);
  digitalWrite(LED_F_PIN, LOW);
  digitalWrite(LED_V_PIN, LOW);
}