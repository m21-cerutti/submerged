//Blinks the onboard LED

#include "f401reMap.h"

int a = pinMap(14); //pinMap(Mapped Pin) - for mapped pin refer the attacted image

void setup()
{
  Serial.begin(115200);
  pinMode(a, OUTPUT);
  Serial.println("Welcome to X-NUCLEO-NFC03A1");
}

void loop()
{
  digitalWrite(a, 1);
  delay(500);
  digitalWrite(a, 0);
  Serial.println("Welcome to X-NUCLEO-NFC03A1");
  delay(500);
  
}
