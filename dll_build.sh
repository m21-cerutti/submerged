#!/usr/bin/env bash

mkdir -p "./Build/"

cp cudnn64_8.dll ./Build/
cp cudnn_cnn_infer64_8.dll ./Build/
cp cudnn_ops_infer64_8.dll ./Build/
cp onnxruntime.dll ./Build/
cp dnn_model_2_0_op11.onnx ./Build/
cp cublas64_11.dll ./Build/
cp cublasLt64_11.dll ./Build/
cp cudart64_110.dll ./Build/
cp cufft64_10.dll ./Build/