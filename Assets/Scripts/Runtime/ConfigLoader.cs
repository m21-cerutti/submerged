using m21cerutti.BUS.Runtime;

using UnityEngine;

[ExecuteInEditMode]
public abstract class ConfigLoader<T, U> : SingletonBehaviour<U> where T : new() where U : SingletonBehaviour<U> {

    public T Config { get; protected set; } = new T();

    protected override void Awake() {
        base.Awake();
        Load();
    }

    public abstract void Load();

    public abstract void Save(bool overwrite = false);

    public abstract string GetPathConfig();
}