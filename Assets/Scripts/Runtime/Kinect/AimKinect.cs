using UnityEngine;

public class AimKinect : MonoBehaviour {
    [SerializeField] private GameObject m_aimAnim;
    [SerializeField] private TrackerHandler m_kinect;
    [SerializeField] private float m_smoothTime = 1;
    [SerializeField] private bool m_keepLastPose;
    [SerializeField] private Vector3 m_restAim = new Vector3(3.5f, 0, 0);

    private Vector3 m_currentVelocity;

    private void Start() {
        if (KinectConfigLoader.instance == null) { enabled = false; }
    }

    private void Update() {
        if (m_kinect.NumBodies > 0) {
            m_aimAnim.transform.position = Vector3.SmoothDamp(m_aimAnim.transform.position,
                                                              m_kinect.Heads[0],
                                                              ref m_currentVelocity,
                                                              m_smoothTime);
        }
        else if (!m_keepLastPose) {
            m_aimAnim.transform.position = Vector3.SmoothDamp(m_aimAnim.transform.position,
                                                              m_restAim,
                                                              ref m_currentVelocity,
                                                              m_smoothTime);
        }
    }
}