﻿using UnityEngine;

public class CameraFlipper : MonoBehaviour {

    [Tooltip("Flip by x axis")] public bool flipByX;
    private Camera cam;

    private void Start() { cam = GetComponent<Camera>(); }

    private void OnPostRender() {
        if (flipByX) { GL.invertCulling = false; }
    }

    // Flip fron camera to be aligned in directions with depth image on scene.
    private void OnPreCull() {
        if (flipByX) {
            cam.ResetWorldToCameraMatrix();
            cam.ResetProjectionMatrix();
            Vector3 scale = new Vector3(-1, 1, 1);
            cam.projectionMatrix = cam.projectionMatrix * Matrix4x4.Scale(scale);
        }
    }

    private void OnPreRender() {
        if (flipByX) { GL.invertCulling = true; }
    }
}