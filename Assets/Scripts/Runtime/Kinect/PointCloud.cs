using System.Threading.Tasks;

using Microsoft.Azure.Kinect.Sensor;

using UnityEngine;
using UnityEngine.Rendering;

public class PointCloud : MonoBehaviour {
    //Array of colors corresponding to each point in PointCloud
    private Color32[] colors;
    //List of indexes of points to be rendered
    private int[] indices;
    //Variable for handling Kinect
    private Device kinect;
    //Used to draw a set of points
    private Mesh mesh;
    //Number of all points of PointCloud 
    private int num;
    //Class for coordinate transformation(e.g.Color-to-depth, depth-to-xyz, etc.)
    private Transformation transformation;
    //Array of coordinates for each point in PointCloud
    private Vector3[] vertices;

    private void OnEnable() {
        //The method to initialize Kinect
        InitKinect();
        //Initialization for point cloud rendering
        InitMesh();
        //Loop to get data from Kinect and rendering
        Task t = KinectLoop();
    }

    //Stop Kinect as soon as this object disappear
    private void OnDisable() { kinect.StopCameras(); }

    //Initialization of Kinect
    private void InitKinect() {
        //Connect with the 0th Kinect
        kinect = Device.Open();
        //Setting the Kinect operation mode and starting it
        kinect.StartCameras(new DeviceConfiguration {
            ColorFormat = ImageFormat.ColorBGRA32,
            ColorResolution = ColorResolution.R720p,
            DepthMode = DepthMode.NFOV_Unbinned,
            SynchronizedImagesOnly = true,
            CameraFPS = FPS.FPS30
        });
        //Access to coordinate transformation information
        transformation = kinect.GetCalibration().CreateTransformation();
    }

    //Prepare to draw point cloud.
    private void InitMesh() {
        //Get the width and height of the Depth image and calculate the number of all points
        int width = kinect.GetCalibration().DepthCameraCalibration.ResolutionWidth;
        int height = kinect.GetCalibration().DepthCameraCalibration.ResolutionHeight;
        num = width * height;

        //Instantiate mesh
        mesh = new Mesh();
        mesh.indexFormat = IndexFormat.UInt32;

        //Allocation of vertex and color storage space for the total number of pixels in the depth image
        vertices = new Vector3[num];
        colors = new Color32[num];
        indices = new int[num];

        //Initialization of index list
        for (int i = 0; i < num; i++) { indices[i] = i; }

        //Allocate a list of point coordinates, colors, and points to be drawn to mesh
        mesh.vertices = vertices;
        mesh.colors32 = colors;
        mesh.SetIndices(indices, MeshTopology.Points, 0);

        gameObject.GetComponent<MeshFilter>().mesh = mesh;
    }

    private async Task KinectLoop() {
        while (isActiveAndEnabled) {
            using (Capture capture = await Task.Run(() => kinect.GetCapture()).ConfigureAwait(true)) {
                //Getting color information
                Image colorImage = transformation.ColorImageToDepthCamera(capture);
                BGRA[] colorArray = colorImage.GetPixels<BGRA>().ToArray();

                //Getting vertices of point cloud
                Image xyzImage = transformation.DepthImageToPointCloud(capture.Depth);
                Short3[] xyzArray = xyzImage.GetPixels<Short3>().ToArray();

                for (int i = 0; i < num; i++) {
                    vertices[i].x = xyzArray[i].X * 0.001f;
                    vertices[i].y = -xyzArray[i].Y * 0.001f; //上下反転
                    vertices[i].z = xyzArray[i].Z * 0.001f;

                    colors[i].b = colorArray[i].B;
                    colors[i].g = colorArray[i].G;
                    colors[i].r = colorArray[i].R;
                    colors[i].a = 255;
                }

                mesh.vertices = vertices;
                mesh.colors32 = colors;
                mesh.RecalculateBounds();
            }
        }
    }
}