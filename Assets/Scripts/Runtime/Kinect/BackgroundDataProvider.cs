using System;
using System.Threading;
using System.Threading.Tasks;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class BackgroundDataProvider : IDisposable {
    private readonly CancellationToken _token;
    private readonly object m_lockObj = new object();
    private CancellationTokenSource _cancellationTokenSource;
    private BackgroundData m_frameBackgroundData = new BackgroundData();
    private bool m_latest;

    public BackgroundDataProvider(int id) {
#if UNITY_EDITOR
        EditorApplication.quitting += OnEditorClose;
#endif
        _cancellationTokenSource = new CancellationTokenSource();
        _token = _cancellationTokenSource.Token;
        Task.Run(() => RunBackgroundThreadAsync(id, _token));
    }

    public bool IsRunning { get; set; } = false;

    public void Dispose() {
#if UNITY_EDITOR
        EditorApplication.quitting -= OnEditorClose;
#endif
        _cancellationTokenSource?.Cancel();
        _cancellationTokenSource?.Dispose();
        _cancellationTokenSource = null;
    }

    private void OnEditorClose() { Dispose(); }

    protected abstract void RunBackgroundThreadAsync(int id, CancellationToken token);

    public void SetCurrentFrameData(ref BackgroundData currentFrameData) {
        lock (m_lockObj) {
            BackgroundData temp = currentFrameData;
            currentFrameData = m_frameBackgroundData;
            m_frameBackgroundData = temp;
            m_latest = true;
        }
    }

    public bool GetCurrentFrameData(ref BackgroundData dataBuffer) {
        lock (m_lockObj) {
            BackgroundData temp = dataBuffer;
            dataBuffer = m_frameBackgroundData;
            m_frameBackgroundData = temp;
            bool result = m_latest;
            m_latest = false;

            return result;
        }
    }
}