﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

using Microsoft.Azure.Kinect.BodyTracking;
using Microsoft.Azure.Kinect.Sensor;

using UnityEngine;

public class SkeletalTrackingProvider : BackgroundDataProvider {
    private TimeSpan initialTimestamp;

    public Stream RawDataLoggingFile = null;
    private bool readFirstFrame;

    public SkeletalTrackingProvider(int id) : base(id) { Debug.Log("in the skeleton provider constructor"); }

    private BinaryFormatter binaryFormatter { get; } = new BinaryFormatter();

    protected override void RunBackgroundThreadAsync(int id, CancellationToken token) {
        try {
            // Buffer allocations.
            BackgroundData currentFrameData = new BackgroundData();
            // Open device.
            using (Device device = Device.Open(id)) {
                device.StartCameras(new DeviceConfiguration {
                    CameraFPS = FPS.FPS30,
                    ColorResolution = ColorResolution.Off,
                    DepthMode = DepthMode.NFOV_Unbinned,
                    WiredSyncMode = WiredSyncMode.Standalone
                });

                HardwareDebugger.instance.HDebug($"Open K4A device successful. id {id} sn: {device.SerialNum}",
                                                 Hardware.Kinect);

                Calibration deviceCalibration = device.GetCalibration();

                using (Tracker tracker = Tracker.Create(deviceCalibration,
                                                        new TrackerConfiguration {
                                                            ProcessingMode = TrackerProcessingMode.Cuda,
                                                            SensorOrientation = SensorOrientation.Default
                                                        })) {
                    HardwareDebugger.instance.HDebug("Body tracker created.", Hardware.Kinect);
                    while (!token.IsCancellationRequested) {
                        using (Capture sensorCapture = device.GetCapture()) {
                            // Queue latest frame from the sensor.
                            tracker.EnqueueCapture(sensorCapture);
                        }

                        // Try getting latest tracker frame.
                        using (Frame frame = tracker.PopResult(new TimeSpan(0, 0, 30), false)) {
                            if (frame == null) {
                                HardwareDebugger.instance.HDebugError("Pop result from tracker timeout!",
                                                                      Hardware.Kinect);
                            }
                            else {
                                IsRunning = true;
                                // Get number of bodies in the current frame.
                                currentFrameData.NumOfBodies = frame.NumberOfBodies;

                                // Copy bodies.
                                for (uint i = 0; i < currentFrameData.NumOfBodies; i++) {
                                    currentFrameData.Bodies[i]
                                                    .CopyFromBodyTrackingSdk(frame.GetBody(i), deviceCalibration);
                                }

                                // Store depth image.
                                Capture bodyFrameCapture = frame.Capture;
                                Image depthImage = bodyFrameCapture.Depth;
                                if (!readFirstFrame) {
                                    readFirstFrame = true;
                                    initialTimestamp = depthImage.DeviceTimestamp;
                                }
                                currentFrameData.TimestampInMs = (float) (depthImage.DeviceTimestamp - initialTimestamp)
                                    .TotalMilliseconds;
                                currentFrameData.DepthImageWidth = depthImage.WidthPixels;
                                currentFrameData.DepthImageHeight = depthImage.HeightPixels;

                                // Read image data from the SDK.
                                Span<ushort> depthFrame = MemoryMarshal.Cast<byte, ushort>(depthImage.Memory.Span);

                                // Repack data and store image data.
                                int byteCounter = 0;
                                currentFrameData.DepthImageSize = currentFrameData.DepthImageWidth *
                                                                  currentFrameData.DepthImageHeight * 3;

                                for (int it = (currentFrameData.DepthImageWidth * currentFrameData.DepthImageHeight) -
                                              1;
                                     it > 0;
                                     it--) {
                                    byte b = (byte) (depthFrame[it] / KinectConfigLoader.instance.Config
                                        .m_skeletalTracking
                                        .m_maximumDisplayedDepthInMillimeters * 255);
                                    currentFrameData.DepthImage[byteCounter++] = b;
                                    currentFrameData.DepthImage[byteCounter++] = b;
                                    currentFrameData.DepthImage[byteCounter++] = b;
                                }

                                if (RawDataLoggingFile != null && RawDataLoggingFile.CanWrite) {
                                    binaryFormatter.Serialize(RawDataLoggingFile, currentFrameData);
                                }

                                // Update data variable that is being read in the UI thread.
                                SetCurrentFrameData(ref currentFrameData);
                            }
                        }
                    }
                    Debug.Log("dispose of tracker now!!!!!");
                    tracker.Dispose();
                }
                device.Dispose();
            }
            if (RawDataLoggingFile != null) { RawDataLoggingFile.Close(); }
        }
        catch (Exception e) {
            Debug.Log($"Stacktrace : {e.Message}\n{e.StackTrace}");
            HardwareDebugger.instance.HDebugError("Catching exception for background thread.", Hardware.Kinect);
            token.ThrowIfCancellationRequested();
        }
    }
}