﻿using System.IO;

using UnityEngine;

[ExecuteInEditMode]
public class KinectConfigLoader : ConfigLoader<KinectConfig, KinectConfigLoader> {

    // Name of scene config file.
    private const string KINECT_DATA_FILE_NAME = "kinect.json";

    public override void Save(bool overwrite = false) {
        if (!overwrite && File.Exists(GetPathConfig())) { return; }
        string data = JsonUtility.ToJson(Config, true);
        File.WriteAllText(GetPathConfig(), data);
    }

    [ContextMenu("Reload config")]
    public override void Load() {
        if (File.Exists(GetPathConfig())) {
            string dataAsJson = File.ReadAllText(GetPathConfig());
            Config = JsonUtility.FromJson<KinectConfig>(dataAsJson);
            Debug.Log("Successfully loaded kinect config file.");
        }
        else { Debug.LogError("Cannot load kinect data!"); }
    }

    public override string GetPathConfig() {
        return Path.Combine(Application.streamingAssetsPath, KINECT_DATA_FILE_NAME);
    }
}