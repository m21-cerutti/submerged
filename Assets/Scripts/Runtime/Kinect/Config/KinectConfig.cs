﻿using System;

using UnityEngine;

[Serializable]
public class SkeletalTracking {
    public float m_maximumDisplayedDepthInMillimeters = 5000;
}

[Serializable]
public class Kinect {
    public Vector3 m_localPosition = new Vector3(0, 0.50f, 0);
    public Vector3 m_localEulerRotation = new Vector3(0, 180, 0);
}

[Serializable]
public class KinectConfig {
    public SkeletalTracking m_skeletalTracking = new SkeletalTracking();
    public Kinect m_kinect = new Kinect();
}