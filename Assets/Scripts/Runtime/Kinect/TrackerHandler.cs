using System;
using System.Collections.Generic;

using Microsoft.Azure.Kinect.BodyTracking;

using UnityEngine;

public class TrackerHandler : MonoBehaviour {
    [SerializeField] private GameObject m_prefabBodyDebug;
    [SerializeField] private int m_maxBodiesTracked = 5;
    [SerializeField] private Matrix4x4 m_transformCorrection = Matrix4x4.identity;

    public Quaternion[] m_absoluteJointRotations = new Quaternion[(int) JointId.Count];
    private readonly Quaternion m_y180Flip = new Quaternion(0.0f, 1.0f, 0.0f, 0.0f);
    private Dictionary<JointId, Quaternion> m_basisJointMap;
    private Dictionary<JointId, JointId> parentJointMap;

    public int NumBodies { get; private set; }

    public Vector3[] Heads { get; private set; }

    // Start is called before the first frame update
    private void Awake() {
        Heads = new Vector3[m_maxBodiesTracked];
        parentJointMap = new Dictionary<JointId, JointId>();

        // pelvis has no parent so set to count
        parentJointMap[JointId.Pelvis] = JointId.Count;
        parentJointMap[JointId.SpineNavel] = JointId.Pelvis;
        parentJointMap[JointId.SpineChest] = JointId.SpineNavel;
        parentJointMap[JointId.Neck] = JointId.SpineChest;
        parentJointMap[JointId.ClavicleLeft] = JointId.SpineChest;
        parentJointMap[JointId.ShoulderLeft] = JointId.ClavicleLeft;
        parentJointMap[JointId.ElbowLeft] = JointId.ShoulderLeft;
        parentJointMap[JointId.WristLeft] = JointId.ElbowLeft;
        parentJointMap[JointId.HandLeft] = JointId.WristLeft;
        parentJointMap[JointId.HandTipLeft] = JointId.HandLeft;
        parentJointMap[JointId.ThumbLeft] = JointId.HandLeft;
        parentJointMap[JointId.ClavicleRight] = JointId.SpineChest;
        parentJointMap[JointId.ShoulderRight] = JointId.ClavicleRight;
        parentJointMap[JointId.ElbowRight] = JointId.ShoulderRight;
        parentJointMap[JointId.WristRight] = JointId.ElbowRight;
        parentJointMap[JointId.HandRight] = JointId.WristRight;
        parentJointMap[JointId.HandTipRight] = JointId.HandRight;
        parentJointMap[JointId.ThumbRight] = JointId.HandRight;
        parentJointMap[JointId.HipLeft] = JointId.SpineNavel;
        parentJointMap[JointId.KneeLeft] = JointId.HipLeft;
        parentJointMap[JointId.AnkleLeft] = JointId.KneeLeft;
        parentJointMap[JointId.FootLeft] = JointId.AnkleLeft;
        parentJointMap[JointId.HipRight] = JointId.SpineNavel;
        parentJointMap[JointId.KneeRight] = JointId.HipRight;
        parentJointMap[JointId.AnkleRight] = JointId.KneeRight;
        parentJointMap[JointId.FootRight] = JointId.AnkleRight;
        parentJointMap[JointId.Head] = JointId.Pelvis;
        parentJointMap[JointId.Nose] = JointId.Head;
        parentJointMap[JointId.EyeLeft] = JointId.Head;
        parentJointMap[JointId.EarLeft] = JointId.Head;
        parentJointMap[JointId.EyeRight] = JointId.Head;
        parentJointMap[JointId.EarRight] = JointId.Head;

        Vector3 zpositive = Vector3.forward;
        Vector3 xpositive = Vector3.right;
        Vector3 ypositive = Vector3.up;
        // spine and left hip are the same
        Quaternion leftHipBasis = Quaternion.LookRotation(xpositive, -zpositive);
        Quaternion spineHipBasis = Quaternion.LookRotation(xpositive, -zpositive);
        Quaternion rightHipBasis = Quaternion.LookRotation(xpositive, zpositive);
        // arms and thumbs share the same basis
        Quaternion leftArmBasis = Quaternion.LookRotation(ypositive, -zpositive);
        Quaternion rightArmBasis = Quaternion.LookRotation(-ypositive, zpositive);
        Quaternion leftHandBasis = Quaternion.LookRotation(-zpositive, -ypositive);
        Quaternion rightHandBasis = Quaternion.identity;
        Quaternion leftFootBasis = Quaternion.LookRotation(xpositive, ypositive);
        Quaternion rightFootBasis = Quaternion.LookRotation(xpositive, -ypositive);

        m_basisJointMap = new Dictionary<JointId, Quaternion>();

        // pelvis has no parent so set to count
        m_basisJointMap[JointId.Pelvis] = spineHipBasis;
        m_basisJointMap[JointId.SpineNavel] = spineHipBasis;
        m_basisJointMap[JointId.SpineChest] = spineHipBasis;
        m_basisJointMap[JointId.Neck] = spineHipBasis;
        m_basisJointMap[JointId.ClavicleLeft] = leftArmBasis;
        m_basisJointMap[JointId.ShoulderLeft] = leftArmBasis;
        m_basisJointMap[JointId.ElbowLeft] = leftArmBasis;
        m_basisJointMap[JointId.WristLeft] = leftHandBasis;
        m_basisJointMap[JointId.HandLeft] = leftHandBasis;
        m_basisJointMap[JointId.HandTipLeft] = leftHandBasis;
        m_basisJointMap[JointId.ThumbLeft] = leftArmBasis;
        m_basisJointMap[JointId.ClavicleRight] = rightArmBasis;
        m_basisJointMap[JointId.ShoulderRight] = rightArmBasis;
        m_basisJointMap[JointId.ElbowRight] = rightArmBasis;
        m_basisJointMap[JointId.WristRight] = rightHandBasis;
        m_basisJointMap[JointId.HandRight] = rightHandBasis;
        m_basisJointMap[JointId.HandTipRight] = rightHandBasis;
        m_basisJointMap[JointId.ThumbRight] = rightArmBasis;
        m_basisJointMap[JointId.HipLeft] = leftHipBasis;
        m_basisJointMap[JointId.KneeLeft] = leftHipBasis;
        m_basisJointMap[JointId.AnkleLeft] = leftHipBasis;
        m_basisJointMap[JointId.FootLeft] = leftFootBasis;
        m_basisJointMap[JointId.HipRight] = rightHipBasis;
        m_basisJointMap[JointId.KneeRight] = rightHipBasis;
        m_basisJointMap[JointId.AnkleRight] = rightHipBasis;
        m_basisJointMap[JointId.FootRight] = rightFootBasis;
        m_basisJointMap[JointId.Head] = spineHipBasis;
        m_basisJointMap[JointId.Nose] = spineHipBasis;
        m_basisJointMap[JointId.EyeLeft] = spineHipBasis;
        m_basisJointMap[JointId.EarLeft] = spineHipBasis;
        m_basisJointMap[JointId.EyeRight] = spineHipBasis;
        m_basisJointMap[JointId.EarRight] = spineHipBasis;
    }

    private void Start() {
        if (KinectConfigLoader.instance != null) {
            transform.localPosition = KinectConfigLoader.instance.Config.m_kinect.m_localPosition;
            transform.localRotation =
                Quaternion.Euler(KinectConfigLoader.instance.Config.m_kinect.m_localEulerRotation);
        }
    }

    [ContextMenu("Set transform config")]
    private void SetConfigTransformKinect() {
        KinectConfigLoader.instance.Config.m_kinect.m_localPosition = transform.localPosition;
        KinectConfigLoader.instance.Config.m_kinect.m_localEulerRotation = transform.localRotation.eulerAngles;
        KinectConfigLoader.instance.Save(true);
    }

    public void UpdateTracker(BackgroundData trackerFrameData) {
        if (trackerFrameData.NumOfBodies < 1) {
            NumBodies = 0;
            if (transform.childCount > 0) { transform.GetChild(0).gameObject.SetActive(false); }

            return;
        }
        // Sort by depth (to not flicker), farther is first
        Array.Sort(trackerFrameData.Bodies,
                   (b1, b2) => {
                       System.Numerics.Vector3 positionB1 = b1.JointPositions3D[(int) JointId.Head];
                       Vector3 pos1 = transform.rotation * (m_transformCorrection * positionB1.ConvertToUnity());
                       System.Numerics.Vector3 positionB2 = b2.JointPositions3D[(int) JointId.Head];
                       Vector3 pos2 = transform.rotation * (m_transformCorrection * positionB2.ConvertToUnity());
                       Vector3 campPos = transform.parent.position;

                       return (pos1 - campPos).z < (pos2 - campPos).z ? -1 :
                           (pos1 - campPos).z > (pos2 - campPos).z ? 1 : 0;
                   });
        int i;

        // create child for debug if not existing
        for (i = 0; i < (int) trackerFrameData.NumOfBodies - transform.childCount && i < m_maxBodiesTracked; i++) {
            Instantiate(m_prefabBodyDebug, transform);
        }

        // render bodies
        for (i = 0; i < (int) trackerFrameData.NumOfBodies && i < transform.childCount; i++) {
            transform.GetChild(i).gameObject.SetActive(true);
            Body skeleton = trackerFrameData.Bodies[i];
            RenderSkeleton(skeleton, i);
        }
        for (; i < transform.childCount; i++) { transform.GetChild(i).gameObject.SetActive(false); }

        NumBodies = (int) trackerFrameData.NumOfBodies;

        //Head track
        /*if (trackerFrameData.NumOfBodies > 0) {
            System.Numerics.Vector3 positionB1 = trackerFrameData.Bodies[0].JointPositions3D[(int) JointId.Head];
            Vector3 pos1 = transform.rotation * (m_transformCorrection * positionB1.ConvertToUnity());
            Vector3 campPos = transform.parent.position;
            Debug.Log((pos1 - campPos).z);
        }*/
        for (i = 0; i < (int) trackerFrameData.NumOfBodies && i < transform.childCount && i < m_maxBodiesTracked; i++) {
            Heads[i] = transform.GetChild(i).GetChild((int) JointId.Head).position;
        }
    }

    private int FindIndexFromId(BackgroundData frameData, int id) {
        int retIndex = -1;
        for (int i = 0; i < (int) frameData.NumOfBodies; i++) {
            if ((int) frameData.Bodies[i].Id == id) {
                retIndex = i;

                break;
            }
        }

        return retIndex;
    }

    private int FindClosestTrackedBody(BackgroundData trackerFrameData) {
        int closestBody = -1;
        const float maxDistance = 5000.0f;
        float minDistanceFromKinect = maxDistance;
        for (int i = 0; i < (int) trackerFrameData.NumOfBodies; i++) {
            System.Numerics.Vector3 pelvisPosition = trackerFrameData.Bodies[i].JointPositions3D[(int) JointId.Pelvis];
            Vector3 pelvisPos = pelvisPosition.ConvertToUnity();
            if (pelvisPos.magnitude < minDistanceFromKinect) {
                closestBody = i;
                minDistanceFromKinect = pelvisPos.magnitude;
            }
        }

        return closestBody;
    }

    public void RenderSkeleton(Body skeleton, int skeletonNumber) {
        for (int jointNum = 0; jointNum < (int) JointId.Count; jointNum++) {
            Vector3 jointPos = m_transformCorrection * skeleton.JointPositions3D[jointNum].ConvertToUnity();
            Vector3 offsetPosition = transform.rotation * jointPos;
            Vector3 positionInTrackerRootSpace = transform.position + offsetPosition;
            Quaternion jointRot = m_y180Flip * new Quaternion(skeleton.JointRotations[jointNum].X,
                                                              skeleton.JointRotations[jointNum].Y,
                                                              skeleton.JointRotations[jointNum].Z,
                                                              skeleton.JointRotations[jointNum].W) *
                                  Quaternion.Inverse(m_basisJointMap[(JointId) jointNum]);
            m_absoluteJointRotations[jointNum] = jointRot;
            // these are absolute body space because each joint has the body root for a parent in the scene graph
            transform.GetChild(skeletonNumber).GetChild(jointNum).localPosition = jointPos;
            transform.GetChild(skeletonNumber).GetChild(jointNum).localRotation = jointRot;

            const int boneChildNum = 0;
            if (parentJointMap[(JointId) jointNum] != JointId.Head &&
                parentJointMap[(JointId) jointNum] != JointId.Count) {
                Vector3 parentTrackerSpacePosition = m_transformCorrection * skeleton
                                                                             .JointPositions3D[
                                                                                 (int) parentJointMap[
                                                                                     (JointId) jointNum]]
                                                                             .ConvertToUnity();
                Vector3 boneDirectionTrackerSpace = jointPos - parentTrackerSpacePosition;
                Vector3 boneDirectionWorldSpace = transform.rotation * boneDirectionTrackerSpace;
                Vector3 boneDirectionLocalSpace =
                    Quaternion.Inverse(transform.GetChild(skeletonNumber).GetChild(jointNum).rotation) *
                    Vector3.Normalize(boneDirectionWorldSpace);
                transform.GetChild(skeletonNumber).GetChild(jointNum).GetChild(boneChildNum).localScale =
                    new Vector3(1, 20.0f * 0.5f * boneDirectionWorldSpace.magnitude, 1);
                transform.GetChild(skeletonNumber).GetChild(jointNum).GetChild(boneChildNum).localRotation =
                    Quaternion.FromToRotation(Vector3.up, boneDirectionLocalSpace);
                transform.GetChild(skeletonNumber).GetChild(jointNum).GetChild(boneChildNum).position =
                    transform.GetChild(skeletonNumber).GetChild(jointNum).position - (0.5f * boneDirectionWorldSpace);
            }
            else {
                transform.GetChild(skeletonNumber).GetChild(jointNum).GetChild(boneChildNum).gameObject
                         .SetActive(false);
            }
        }
    }

    public Quaternion GetRelativeJointRotation(JointId jointId) {
        JointId parent = parentJointMap[jointId];
        Quaternion parentJointRotationBodySpace = Quaternion.identity;
        if (parent == JointId.Count) { parentJointRotationBodySpace = m_y180Flip; }
        else { parentJointRotationBodySpace = m_absoluteJointRotations[(int) parent]; }
        Quaternion jointRotationBodySpace = m_absoluteJointRotations[(int) jointId];
        Quaternion relativeRotation = Quaternion.Inverse(parentJointRotationBodySpace) * jointRotationBodySpace;

        return relativeRotation;
    }
}