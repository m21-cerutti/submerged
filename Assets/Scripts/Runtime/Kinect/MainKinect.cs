﻿using UnityEngine;

public class MainKinect : MonoBehaviour {
    // Handler for SkeletalTracking thread.
    public GameObject m_tracker;
    public BackgroundData m_lastFrameData = new BackgroundData();
    private SkeletalTrackingProvider m_skeletalTrackingProvider;

    private void Start() {
        //tracker ids needed for when there are two trackers
        const int TRACKER_ID = 0;
        m_skeletalTrackingProvider = new SkeletalTrackingProvider(TRACKER_ID);
    }

    private void Update() {
        if (m_skeletalTrackingProvider.IsRunning) {
            if (m_skeletalTrackingProvider.GetCurrentFrameData(ref m_lastFrameData)) {
                m_tracker.GetComponent<TrackerHandler>().UpdateTracker(m_lastFrameData);
            }
        }
    }

    private void OnApplicationQuit() {
        if (m_skeletalTrackingProvider != null) { m_skeletalTrackingProvider.Dispose(); }
    }
}