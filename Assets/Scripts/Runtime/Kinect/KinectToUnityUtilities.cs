using UnityEngine;

public static class KinectToUnityUtilities {
    public static Vector3 ConvertToUnity(this System.Numerics.Vector3 myVector) {
        return new Vector3(myVector.X, -myVector.Y, myVector.Z);
    }
}