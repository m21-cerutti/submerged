using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;

[ExecuteInEditMode]
public class MonitorConfigLoader : ConfigLoader<MonitorConfig, MonitorConfigLoader> {

    // Name of scene config file.
    private const string MONITOR_DATA_FILE_NAME = "monitor.json";
    public Dictionary<string, Camera> camDisplay = new Dictionary<string, Camera>();

    private void GetCameras() {
        Camera[] cameras = FindObjectsOfType<Camera>();
        camDisplay = new Dictionary<string, Camera>();
        foreach (Camera c in cameras) { camDisplay.Add(c.gameObject.name, c); }
    }

    public override void Save(bool overwrite = false) {
        if (!overwrite && File.Exists(GetPathConfig())) { return; }
        Config.m_displays = camDisplay.Select(x => new Monitor(x.Key, x.Value.targetDisplay)).ToList();
        string data = JsonUtility.ToJson(Config, true);
        File.WriteAllText(GetPathConfig(), data);
    }

    [ContextMenu("Reload config")]
    public override void Load() {
        GetCameras();
        if (File.Exists(GetPathConfig())) {
            string dataAsJson = File.ReadAllText(GetPathConfig());
            Config = JsonUtility.FromJson<MonitorConfig>(dataAsJson);
            Debug.Log("Successfully loaded monitor config file.");
        }
        else { Debug.LogError("Cannot load monitor data!"); }

#if !UNITY_EDITOR
        if (Display.displays.Length < 3) { Debug.LogError("Not enough screens"); }
#endif
        // Camera parameters
        foreach (Monitor monitor in Config.m_displays) {
            Debug.Log($"{monitor.m_camera} {monitor.m_screen}");
            camDisplay[monitor.m_camera].targetDisplay = monitor.m_screen;
#if !UNITY_EDITOR
            Display.displays[monitor.m_screen].Activate();
#endif
        }
    }

    public override string GetPathConfig() {
        return Path.Combine(Application.streamingAssetsPath, MONITOR_DATA_FILE_NAME);
    }
}