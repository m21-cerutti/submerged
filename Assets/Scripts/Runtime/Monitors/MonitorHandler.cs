using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UIElements;

[ExecuteInEditMode]
public class MonitorHandler : MonoBehaviour {

    [SerializeField] private PanelSettings m_ui;

    private readonly List<Vector3> m_mjPositions = new List<Vector3>();
    private readonly List<Quaternion> m_mjRotations = new List<Quaternion>();
    private int m_selectedCamMj;

    private void Start() {
        m_ui.targetDisplay = MonitorConfigLoader.instance.camDisplay["MJScreen"].targetDisplay;
        InitCamPositions();
    }

    [ContextMenu("Reload cams")]
    private void InitCamPositions() {
        // Get positions and rotations
        m_mjPositions.Clear();
        m_mjRotations.Clear();
        foreach (KeyValuePair<string, Camera> c in MonitorConfigLoader.instance.camDisplay) {
            m_mjPositions.Add(c.Value.transform.position);
            m_mjRotations.Add(c.Value.transform.rotation);
        }
    }

    public void ChangeCamMj() {
        m_selectedCamMj = (m_selectedCamMj + 1) % m_mjPositions.Count;
        MonitorConfigLoader.instance.camDisplay["MJScreen"].transform.position = m_mjPositions[m_selectedCamMj];
        MonitorConfigLoader.instance.camDisplay["MJScreen"].transform.rotation = m_mjRotations[m_selectedCamMj];
    }
}