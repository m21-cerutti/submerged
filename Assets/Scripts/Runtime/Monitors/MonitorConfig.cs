using System;
using System.Collections.Generic;

[Serializable]
public class Monitor {

    public string m_camera;
    public int m_screen;

    public Monitor(string camera, int screen) {
        m_camera = camera;
        m_screen = screen;
    }
}

[Serializable]
public class MonitorConfig {
    public List<Monitor> m_displays = new List<Monitor>();
}