﻿namespace Runtime.Enums {

    public static class ComparerFunctions {
        public static bool TestCompare(Comparer c, int reference, int value) {
            switch (c) {
                case Comparer.equal: return reference == value;

                case Comparer.above: return value < reference;

                case Comparer.below: return value > reference;
            }

            return false;
        }
    }
}