public enum StateGame {
		NotInit,
		Start,
		Opened,
		Liberated,
		TreasureFound,
		TreasureUnlocked,
}
