    using UnityEngine;
    using UnityEngine.Rendering;
     
    //https://forum.unity.com/threads/errors-with-the-urp-debug-manager.987795/
    public class DisableURPDebugUpdater : MonoBehaviour
    {
        private void Awake()
        {
            DebugManager.instance.enableRuntimeUI = false;
        }
    }
