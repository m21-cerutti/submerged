using UnityEngine;

[ExecuteAlways]
public class RotateTime : MonoBehaviour {
    [SerializeField] private float m_factor = 50.0f;

    private void Update() { transform.Rotate(Vector3.up, m_factor * Time.deltaTime); }
}