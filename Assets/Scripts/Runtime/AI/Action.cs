using System;

using UnityEngine;

[Serializable]
public abstract class Action {
    [SerializeField] protected string m_name;

#if UNITY_EDITOR
    protected Action(string name) { m_name = name; }
#endif

    public abstract void Exec(GameController controller);
}