using System;

using UnityEngine;

[Serializable]
public class StateChangeAction : Action {
    [SerializeField] private StateGame m_state;

#if UNITY_EDITOR
    public StateChangeAction(string name, StateGame state) : base(name) { m_state = state; }
#endif

    public override void Exec(GameController controller) { controller.CurrentState = m_state; }
}