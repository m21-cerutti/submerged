using System;

using m21cerutti.ClapClapEvent.CustomEvents;

using UnityEngine;

[Serializable]
public class EventAction : Action {

    [SerializeField] private DelegateTimerEvent m_event;

#if UNITY_EDITOR
    public EventAction(string name, DelegateTimerEvent ev) : base(name) { m_event = ev; }
#endif

    public override void Exec(GameController controller) { m_event.Invoke(controller.SetWaitEndAi); }
}