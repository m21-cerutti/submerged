using System;

using Runtime.Enums;

using UnityEngine;

[Serializable]
public class TimeSinceLastStateCondition : Condition {
    [SerializeField] private Comparer m_comparer;
    [SerializeField] private int m_time;
    [SerializeField] private bool m_is_trigered;

#if UNITY_EDITOR
    public TimeSinceLastStateCondition(string name, Comparer comparer, int time) :
        base(name) {
        m_comparer = comparer;
        m_time = time;
    }
#endif

    public override bool Evaluate(GameController controller) {
        bool triggered =
            ComparerFunctions.TestCompare(m_comparer,
                                          Mathf.RoundToInt(TimeSingleton.instance.TimeSinceLastState),
                                          m_time);
        if (m_comparer != Comparer.equal) { return triggered; }
        if (triggered && m_is_trigered) { return false; }
        m_is_trigered = triggered;

        return triggered;
    }
}