using System;

using UnityEngine;

[Serializable]
public class ObjectDetectionCondition : Condition {
    [SerializeField] private PhysicObject m_object;
    [SerializeField] private Detection m_mode;

#if UNITY_EDITOR
    public ObjectDetectionCondition(string name, PhysicObject physicObject, Detection mode) :
        base(name) {
        m_object = physicObject;
        m_mode = mode;
    }
#endif

    public override bool Evaluate(GameController controller) {
        if (m_mode == Detection.OnDeck) { return controller.ObjectOnDeck == m_object; }
        if (TimeSingleton.instance.NbFramesObjectOnDeck != 0) { return false; }
        switch (m_mode) {
            case Detection.Placed: return controller.ObjectOnDeck == m_object;

            case Detection.Removed: return controller.LastObjectOnDeck == m_object;
        }

        return false;
    }
}