using System;

using UnityEngine;

[Serializable]
public class StateCondition : Condition {
    [SerializeField] private StateGame m_state;

#if UNITY_EDITOR
    public StateCondition(string name, StateGame state) : base(name) { m_state = state; }
#endif

    public override bool Evaluate(GameController controller) { return controller.CurrentState == m_state; }
}