using System.Collections.Generic;

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "BehaviourContainer", menuName = "BehaviourContainer")]
public class BehaviourContainer : ScriptableObject {
    [SerializeField] private List<AiBehaviour> m_behaviours;

    public void Init(GameController controller) { m_behaviours[0].Exec(controller); }

    public void Exec(GameController controller) {
        foreach (AiBehaviour behaviour in m_behaviours) {
            if (behaviour.Exec(controller)) { break; }
        }
    }

    public void Debug(GameController controller) {
        controller.Print("<- List behaviour ids");
        foreach (AiBehaviour behaviour in m_behaviours) { controller.Print($"{behaviour.ID} \t {behaviour.Name}"); }

        controller.Print("---");
    }

#if UNITY_EDITOR
    public void FillContainer(List<AiBehaviour> b) {
        m_behaviours = b;
        m_behaviours.Sort((x, y) => x.ID.CompareTo(y.ID));
        EditorUtility.SetDirty(this);
    }
#endif
}