using System;

using UnityEngine;

[Serializable]
public abstract class Condition {
    [SerializeField] protected string m_name;

#if UNITY_EDITOR
    protected Condition(string name) { m_name = name; }
#endif

    public abstract bool Evaluate(GameController controller);
}