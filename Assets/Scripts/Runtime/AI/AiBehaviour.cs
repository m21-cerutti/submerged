using System;
using System.Collections.Generic;

using UnityEngine;

[Serializable]
public class AiBehaviour {
    [SerializeField] private string m_name = "";
    [SerializeField] private int m_id;
    [SerializeField] private KeyCode m_override;
    [SerializeReference] private List<Condition> m_conditions = new List<Condition>();
    [SerializeReference] private List<Action> m_actions = new List<Action>();

    public int    ID           => m_id;
    public string Name         => m_name;
    public int    NbConditions => m_conditions.Count;

    public bool Exec(GameController controller) {
        if (Input.anyKey && !Input.GetKeyDown(m_override)) { return false; }

        bool inputPressed = Input.GetKeyDown(m_override);
        if (inputPressed) {
            switch (m_conditions.Count) {
                case 0:
                    controller.Print($" <- Trigger manual {m_override}");

                    break;

                case > 0 when m_conditions[0] is StateCondition && m_conditions[0].Evaluate(controller):
                    controller.Print($" <- Override {m_override}");

                    break;

                default: return false;
            }
        }
        else {
            if (m_conditions.Count == 0) { return false; }
            if (m_conditions.Count == 1 && m_conditions[0] is StateCondition) { return false; }
            foreach (Condition c in m_conditions) {
                if (c.Evaluate(controller) == false) { return false; }
            }
        }

        foreach (Action c in m_actions) { c.Exec(controller); }

        controller.Print($"Action {Name}({ID}) triggered.");

        if (ID < 1) { TimeSingleton.instance.ResetGlobalTimer(); }

        return true;
    }

#if UNITY_EDITOR
    public AiBehaviour(int id, KeyCode key, string name) {
        m_id = id;
        m_override = key;
        m_name = name;
    }

    public void AddCondition(Condition c) { m_conditions.Add(c); }

    public void AddAction(Action a) { m_actions.Add(a); }
#endif
}