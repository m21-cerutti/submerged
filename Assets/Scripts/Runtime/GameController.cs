using System.Collections;

using m21cerutti.BUS.Runtime;
using m21cerutti.ClapClapEvent.CustomEvents;

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameController : SingletonBehaviour<GameController> {
    [Header("State")]
    [SerializeField]
    [ReadOnlyFieldAttribute]
    private StateGame m_currentState;
    [SerializeField]
    [ReadOnlyFieldAttribute]
    private StateGame m_lastState;
    [SerializeField]
    [ReadOnlyFieldAttribute]
    private PhysicObject m_objectOnDeck;
    [SerializeField]
    [ReadOnlyFieldAttribute]
    private PhysicObject m_lastObjectOnDeck;

    [Header("Components")]
    [SerializeField]
    private BehaviourContainer m_container;
    [SerializeField] private StringEvent m_debugAi;
    [SerializeField] private StateGameEvent m_currentStateChanged;

    private float m_waitLoopAi;

    public StateGame CurrentState {
        get => m_currentState;
        set {
            m_currentState = value;
            TimeSingleton.instance.ResetStateTimer();
        }
    }

    public PhysicObject ObjectOnDeck {
        get => m_objectOnDeck;
        private set {
            if (m_objectOnDeck != value) {
                m_lastObjectOnDeck = m_objectOnDeck;
                TimeSingleton.instance.ResetNbFramesObjectOnDeck();
                m_objectOnDeck = value;
            }
        }
    }

    public PhysicObject LastObjectOnDeck => m_lastObjectOnDeck;

    private void Start() {
        m_container.Debug(this);
        StartCoroutine(UpdateAILoop());
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
#if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
#endif
            Application.Quit();
        }
    }

    private IEnumerator UpdateAILoop() {
        yield return new WaitForEndOfFrame();
        m_container.Init(this);

        yield return new WaitForEndOfFrame();
        while (true) {
            m_container.Exec(this);

            yield return new WaitForEndOfFrame();
            yield return new WaitForSecondsRealtime(m_waitLoopAi);

            if (m_lastState != m_currentState) {
                m_lastState = m_currentState;
                m_currentStateChanged.Invoke(m_currentState);
            }

            m_waitLoopAi = 0;
        }
    }

    public void SetWaitEndAi(float timer) { m_waitLoopAi = Mathf.Max(m_waitLoopAi, timer); }

    public void OnPhysicObject(PhysicObject obj) { ObjectOnDeck = obj; }

    public void Print(string message) {
        Debug.Log(message);
        m_debugAi.Invoke(message);
    }
}