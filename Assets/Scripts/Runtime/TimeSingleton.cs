using m21cerutti.BUS.Runtime;

using UnityEngine;

public class TimeSingleton : SingletonBehaviour<TimeSingleton> {
    [SerializeField]
    [ReadOnlyFieldAttribute]
    private ulong m_nbFramesObjectOnDeck;
    [SerializeField]
    [ReadOnlyFieldAttribute]
    private float m_timeSinceLastState;
    [SerializeField]
    [ReadOnlyFieldAttribute]
    private float m_globalTimer;

    public ulong NbFramesObjectOnDeck => m_nbFramesObjectOnDeck;
    public float TimeSinceLastState   => m_timeSinceLastState;
    public float GlobalTimer          => m_globalTimer;

    private void Update() {
        m_nbFramesObjectOnDeck++;
        m_timeSinceLastState += Time.deltaTime;
        m_globalTimer += Time.deltaTime;
    }

    public void ResetGlobalTimer()          { m_globalTimer = 0; }
    public void ResetStateTimer()           { m_timeSinceLastState = 0; }
    public void ResetNbFramesObjectOnDeck() { m_nbFramesObjectOnDeck = 0; }
}