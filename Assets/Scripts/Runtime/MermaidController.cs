using System;

using m21cerutti.ClapClapEvent.CustomEvents;

using UnityEngine;
#if UNITY_EDITOR
using System.Collections.Generic;

using UnityEditor.Animations;
#endif

[Serializable]
public class LengthClip {

    public string m_name;
    public float m_length;

    public LengthClip(string name, float length) {
        m_name = name;
        m_length = length;
    }
}

[Serializable]
public class TransitionHash {

    public string m_name;
    public int m_hash;

    public TransitionHash(string name, int hash) {
        m_name = name;
        m_hash = hash;
    }
}

[DefaultExecutionOrder(0)]
public class MermaidController : MonoBehaviour {

    [Header("Animations")]
    [SerializeField] private Animator m_animatorMermaid;
    [SerializeField] private LengthClip[] m_animLengthMermaid;
    [SerializeField] private float m_speedCount = 1;
    [SerializeField] private float m_resetTimerCount = 5.0f;

    [Header("Movements")]
    [SerializeField] private Animator m_animatorMoves;
    [SerializeField] private LengthClip[] m_animLengthMoves;

    private readonly int m_countChangedTrigger = Animator.StringToHash("CountChanged");
    private readonly int m_countFloat = Animator.StringToHash("Count");
    private readonly int m_endChoice = Animator.StringToHash("EndChoice");
    private readonly int m_freeTrigger = Animator.StringToHash("Free");
    private readonly int m_headTurnTrigger = Animator.StringToHash("HeadTurn");
    private readonly int m_hiddenState = Animator.StringToHash("Hidden");
    private readonly int m_hideTrigger = Animator.StringToHash("Hide");
    private readonly int m_resetDilemmaTrigger = Animator.StringToHash("ResetDilemma");
    private readonly int m_resetFreeTrigger = Animator.StringToHash("ResetFree");
    private readonly int m_resetTrigger = Animator.StringToHash("Reset");
    private readonly int m_showTrigger = Animator.StringToHash("Show");
    private readonly int m_swimState = Animator.StringToHash("Swim");
    private readonly int m_tGetTrigger = Animator.StringToHash("TreasureGet");
    private readonly int m_tSeeTrigger = Animator.StringToHash("TreasureSee");
    private readonly int m_turnTrigger = Animator.StringToHash("Turn180");

    private HeadAimIK m_ikHeads;

    private float m_timerCount = -1;
    private float m_count = -1;
    private float m_aimCount = -1;

    private void Start() { m_ikHeads = GetComponent<HeadAimIK>(); }

    private void Update() {
        // Count
        if (m_timerCount > 0) { m_timerCount -= Time.deltaTime; }
        else if (m_timerCount < 0) {
            m_aimCount = -1;
            m_timerCount = 0;
        }
        m_count = Mathf.Lerp(m_count, m_aimCount, Time.deltaTime * m_speedCount);
        if (m_count > -1) { m_animatorMermaid.SetFloat(m_countFloat, m_count); }
    }

    #region Events
    public void OnResetAI(DelegateTimer timer) {
        m_animatorMoves.ResetAllAnimatorParameters();
        m_animatorMermaid.ResetAllAnimatorParameters();
        m_animatorMermaid.SetTrigger(m_resetTrigger);
        m_animatorMoves.SetTrigger(m_resetTrigger);
        DeactivateHeadTrack();
        m_animatorMermaid.SetInteger(m_endChoice, 0);
        OnResetShowAI(timer);
    }

    public void OnQuickLookAI(DelegateTimer timer) {
        m_animatorMermaid.SetTrigger(m_headTurnTrigger);
        timer(m_animLengthMermaid[0].m_length);
    }

    // Move after freedom
    public void OnReleaseAI(DelegateTimer timer) {
        DeactivateHeadTrack();
        m_animatorMermaid.SetTrigger(m_freeTrigger);
        float animTime = m_animLengthMermaid[1].m_length + m_animLengthMoves[0].m_length;
        timer(animTime);
    }

    // Move after turn
    public void OnHideAI(DelegateTimer timer) {
        DeactivateHeadTrack();
        m_animatorMermaid.SetTrigger(m_turnTrigger);
        m_animatorMermaid.SetBool(m_hiddenState, true);
        float animTime = m_animLengthMermaid[3].m_length + m_animLengthMoves[1].m_length;
        timer(animTime);
    }

    public void OnComeBackAI(DelegateTimer timer) {
        m_animatorMoves.ResetAllAnimatorParameters();
        m_animatorMermaid.ResetAllAnimatorParameters();
        DeactivateHeadTrack();
        m_animatorMoves.SetTrigger(m_showTrigger);
        m_animatorMermaid.SetBool(m_hiddenState, false);
        MoveShow();
        float animTime = m_animLengthMoves[2].m_length;
        timer(animTime);
    }

    public void OnResetFreeAI(DelegateTimer timer) {
        m_animatorMermaid.SetTrigger(m_resetFreeTrigger);
        m_animatorMoves.SetTrigger(m_resetFreeTrigger);
        ActivateHeadTrack();
        m_animatorMermaid.SetInteger(m_endChoice, 0);
        OnResetShowAI(timer);
    }

    private void OnShowNumberAI(DelegateTimer timer, int number) {
        if ((int) m_aimCount == number) { return; }
        m_aimCount = number;
        m_animatorMermaid.SetTrigger(m_countChangedTrigger);
        m_timerCount = number < 0 ? 0 : m_resetTimerCount;
    }

    public void OnShow1AI(DelegateTimer timer) { OnShowNumberAI(timer, 1); }

    public void OnShow2AI(DelegateTimer timer) { OnShowNumberAI(timer, 2); }

    public void OnShow3AI(DelegateTimer timer) { OnShowNumberAI(timer, 3); }

    public void OnShow4AI(DelegateTimer timer) { OnShowNumberAI(timer, 4); }

    public void OnShow5AI(DelegateTimer timer) { OnShowNumberAI(timer, 5); }

    public void OnResetShowAI(DelegateTimer timer) { OnShowNumberAI(timer, -1); }

    public void OnTreasureFoundAI(DelegateTimer timer) {
        ActivateHeadTrack();
        m_animatorMermaid.SetTrigger(m_tSeeTrigger);
        float animTime = m_animLengthMermaid[4].m_length;
        timer(animTime);
    }

    public void OnTreasureUnlockedAI(DelegateTimer timer) {
        m_animatorMoves.SetTrigger(m_resetFreeTrigger);
        m_animatorMermaid.SetTrigger(m_resetDilemmaTrigger);
        m_animatorMermaid.SetInteger(m_endChoice, 0);
        ActivateHeadTrack();
        OnResetShowAI(timer);
    }

    public void OnTreasureGetAI(DelegateTimer timer) {
        m_animatorMermaid.SetTrigger(m_tGetTrigger);
        //float animTime = m_animLengthMermaid[4].m_length;
        //timer(animTime);
    }

    public void OnKeepAI(DelegateTimer timer) {
        ActivateHeadTrack();
        m_animatorMermaid.SetInteger(m_endChoice, -1);
        //float animTime = m_animLengthMermaid[5].m_length;
        //timer(animTime);
    }

    public void OnGiveAI(DelegateTimer timer) {
        DeactivateHeadTrack();
        m_animatorMermaid.SetInteger(m_endChoice, 1);
        //float animTime = m_animLengthMermaid[6].m_length;
        //timer(animTime);
    }
    #endregion

    #region Anims motions
    public void MoveHide() { m_animatorMoves.SetTrigger(m_hideTrigger); }

    public void MoveShow() { m_animatorMoves.SetTrigger(m_showTrigger); }

    public void MoveFree() { m_animatorMoves.SetTrigger(m_freeTrigger); }

    public void OnMove() { m_animatorMermaid.SetBool(m_swimState, true); }

    public void OnStopMove() {
        m_animatorMermaid.SetBool(m_swimState, false);
        ActivateHeadTrack();
    }
    #endregion

    #region Head track
    public void ActivateHeadTrack() { m_ikHeads.SetTrack(true); }

    public void DeactivateHeadTrack() { m_ikHeads.SetTrack(false); }
    #endregion

#if UNITY_EDITOR
    [ContextMenu("Populate moves animations")]
    private void PopulateAnimationMovesClips() {
        AnimatorController controller = m_animatorMoves.runtimeAnimatorController as AnimatorController;
        List<LengthClip> clips = new List<LengthClip>();
        int i = 0;
        foreach (ChildAnimatorState stateAnim in controller.layers[0].stateMachine.states) {
            if (!stateAnim.state.motion.isLooping) {
                clips.Add(new LengthClip($"{i}_{stateAnim.state.name}",
                                         stateAnim.state.motion.averageDuration));
                i++;
            }
        }
        m_animLengthMoves = clips.ToArray();
    }

    [ContextMenu("Populate mermaid animations")]
    private void PopulateAnimationMermaidClips() {
        AnimatorController controller = m_animatorMermaid.runtimeAnimatorController as AnimatorController;
        List<LengthClip> clips = new List<LengthClip>();
        int i = 0;
        foreach (ChildAnimatorState stateAnim in controller.layers[0].stateMachine.states) {
            if (!stateAnim.state.motion.isLooping) {
                clips.Add(new LengthClip($"{i}_{stateAnim.state.name}",
                                         stateAnim.state.motion.averageDuration));
                i++;
            }
        }
        m_animLengthMermaid = clips.ToArray();
    }
#endif
}