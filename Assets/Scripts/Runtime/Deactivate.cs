using UnityEngine;

public class Deactivate : MonoBehaviour {
    [SerializeField] private MonoBehaviour component;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            if (component == null) { gameObject.SetActive(!gameObject.activeSelf); }
            else { component.enabled = !component.enabled; }
        }
    }
}