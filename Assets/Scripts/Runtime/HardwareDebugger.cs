using System.Collections.Concurrent;

using m21cerutti.BUS.Runtime;
using m21cerutti.ClapClapEvent.CustomEvents;

using UnityEngine;

public enum Hardware {
    Arduino,
    Kinect
}

[DefaultExecutionOrder(-1)]
public class HardwareDebugger : SingletonBehaviour<HardwareDebugger> {
    [SerializeField] private StringEvent m_debugHardwareText;

    private readonly ConcurrentQueue<string> m_logQueue = new ConcurrentQueue<string>();

    private void Update() {
        while (!m_logQueue.IsEmpty) {
            m_logQueue.TryDequeue(out string log);
            m_debugHardwareText.Invoke(log);
        }
    }

    public void HDebug(string message, Hardware hardware) {
        string log = $"{hardware} : {message}";
        Debug.Log(log);
        m_logQueue.Enqueue(log);
    }

    public void HDebugWarn(string message, Hardware hardware) {
        string log = $"WARN {hardware} : {message}";
        Debug.LogWarning(log);
        m_logQueue.Enqueue(log);
    }

    public void HDebugError(string message, Hardware hardware) {
        string log = $"ERROR {hardware} : {message}";
        Debug.LogWarning(log);
        m_logQueue.Enqueue(log);
    }
}