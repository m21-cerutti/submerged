using System;
using System.Text;

using m21cerutti.ClapClapEvent.CustomEvents;
using m21cerutti.ClapClapEvent.Runtime;

using UnityEngine;
using UnityEngine.UIElements;

public class MenuMj : MonoBehaviour {

    [SerializeField] private SimpleEvent m_testArduino;
    [SerializeField] private BoolEvent m_registerArduino;
    [SerializeField] private BoolEvent m_mute;

    private readonly StringBuilder m_debugAi = new StringBuilder();
    private readonly StringBuilder m_debugHardware = new StringBuilder();
    private Label m_aiDebugUi;
    private VisualElement m_boxDebug;
    private Label m_currentStateUi;
    private Label m_hardwareDebugUi;
    private MonitorHandler m_monitors;
    private Label m_objectUi;
    private Label m_timerGlobalUi;
    private Label m_timerLastStateUi;
    private Label m_timerStateUi;

    private void Awake() {
        m_monitors = GetComponentInParent<MonitorHandler>();
        VisualElement root = GetComponent<UIDocument>().rootVisualElement;
        m_boxDebug = root.Q<VisualElement>("box-debug");
        m_timerGlobalUi = root.Q<Label>("timer");
        m_timerStateUi = root.Q<Label>("timer-state");
        m_objectUi = root.Q<Label>("object-deck");
        m_currentStateUi = root.Q<Label>("current-state");
        m_hardwareDebugUi = root.Q<Label>("debug-hardware");
        m_aiDebugUi = root.Q<Label>("debug-ai");
        m_hardwareDebugUi.text = "";
        m_aiDebugUi.text = "";
        root.Q<Button>("show-btn").clicked += () => {
            m_boxDebug.visible = !m_boxDebug.visible;
        };
        root.Q<Button>("test-connect-btn").clicked += () => {
            m_testArduino.Invoke();
        };

        root.Q<Button>("change-cam").clicked += () => {
            m_monitors.ChangeCamMj();
        };
        root.Q<Toggle>("register-mode").RegisterValueChangedCallback(x => m_registerArduino.Invoke(x.newValue));
        root.Q<Toggle>("mute").RegisterValueChangedCallback(x => m_mute.Invoke(x.newValue));
        root.Q<Button>("clear-ia").clicked += () => {
            m_debugAi.Clear();
            m_aiDebugUi.text = "";
        };
        root.Q<Button>("clear-hardware").clicked += () => {
            m_debugHardware.Clear();
            m_hardwareDebugUi.text = "";
        };
    }

    private void Update() {
        m_timerGlobalUi.text = $"{TimeSpan.FromSeconds(TimeSingleton.instance.GlobalTimer):mm\\:ss}";
        m_timerStateUi.text = $"{TimeSpan.FromSeconds(TimeSingleton.instance.TimeSinceLastState):mm\\:ss}";
    }

    private void OnDebugHardware(string text) {
        m_debugHardware.Insert(0, text + "\n");
        m_hardwareDebugUi.text = m_debugHardware.ToString();
    }

    private void OnDebugAi(string text) {
        m_debugAi.Insert(0, text + "\n");
        m_aiDebugUi.text = m_debugAi.ToString();
    }

    private void OnPhysicObject(PhysicObject obj) { m_objectUi.text = $"{obj}"; }
    private void OnStateGameChanged(StateGame s)  { m_currentStateUi.text = $"{s}"; }
}