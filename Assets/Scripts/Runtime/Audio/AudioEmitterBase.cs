using System.Collections.Generic;
using System.Reflection;

using FMOD.Studio;

using FMODUnity;

using UnityEngine;

public abstract class AudioEmitterBase : MonoBehaviour {
    private Dictionary<EventReference, EventInstance> m_eventInstances;

    protected void Awake() { m_eventInstances = new Dictionary<EventReference, EventInstance>(); }

    protected virtual void Start() { InitAllEvents(); }

    private void InitAllEvents() {
        FieldInfo[] fields =
            GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        foreach (FieldInfo fieldInfo in fields) {
            if (fieldInfo.FieldType == typeof(EventReference)) {
                EventReference eventName = (EventReference) fieldInfo.GetValue(this);
                EventInstance eventInstance = RuntimeManager.CreateInstance(eventName);
                eventInstance.set3DAttributes(transform.To3DAttributes());
                m_eventInstances.Add(eventName, eventInstance);
            }
            else if (fieldInfo.FieldType == typeof(EventReference[])) {
                EventReference[] eventNames = (EventReference[]) fieldInfo.GetValue(this);
                foreach (EventReference eventName in eventNames) {
                    EventInstance eventInstance = RuntimeManager.CreateInstance(eventName);
                    eventInstance.set3DAttributes(transform.To3DAttributes());
                    m_eventInstances.Add(eventName, eventInstance);
                }
            }
        }
    }

    protected EventInstance GetEventInstance(EventReference eventRef) { return m_eventInstances[eventRef]; }

    protected static void ChangeGlobalParameter(string parameterName, float value) {
        RuntimeManager.StudioSystem.setParameterByName(parameterName, value);
    }

    protected void ChangeLocalParameter(EventReference eventRef, string parameterName, float value) {
        GetEventInstance(eventRef).setParameterByName(parameterName, value);
    }

    protected float GetGlobalParameter(EventReference eventRef, string parameterName) {
        RuntimeManager.StudioSystem.getParameterByName(parameterName, out float result);

        return result;
    }

    protected float GetLocalParameter(EventReference eventRef, string parameterName) {
        GetEventInstance(eventRef).getParameterByName(parameterName, out float result);

        return result;
    }
}