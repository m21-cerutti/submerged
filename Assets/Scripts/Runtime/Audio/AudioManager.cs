using m21cerutti.BUS.Runtime;

using UnityEngine;

[ExecuteInEditMode]
public class AudioManager : SingletonBehaviour<AudioManager> {
    private AudioGlobalSounds m_globalSounds;

    protected override void Awake() {
        base.Awake();
        m_globalSounds = GetComponent<AudioGlobalSounds>();
        if (m_globalSounds == null) { m_globalSounds = gameObject.AddComponent<AudioGlobalSounds>(); }
    }
}