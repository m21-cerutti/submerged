using System.Collections;

using FMOD;
using FMOD.Studio;

using FMODUnity;

using m21cerutti.ClapClapEvent.CustomEvents;

using UnityEngine;

using Debug = UnityEngine.Debug;
using STOP_MODE = FMOD.Studio.STOP_MODE;

[DefaultExecutionOrder(-1)]
public class AudioMermaid : AudioEmitterBase {
    [SerializeField] private string m_pathBusMermaid = "bus:/Mermaid";

    [Header("Sob begin")]
    [SerializeField] [Range(1, 10)] private float m_waitVocals;
    [SerializeField] [Range(1, 100)] private float m_vocalsProba;

    [Header("Events")]
    [SerializeField] private EventReference m_vocalise;
    [SerializeField] private EventReference m_gaspHeadTurn;
    [SerializeField] private EventReference m_gaspSurprise;
    [SerializeField] private EventReference m_hmmm;
    [SerializeField] private EventReference m_freedomShot;
    [SerializeField] private EventReference m_hmmmNegative;
    [SerializeField] private EventReference m_negativeCompting;
    [SerializeField] private EventReference m_sight;
    [SerializeField] private EventReference m_dilemma;
    [SerializeField] private EventReference m_validCompting;
    [SerializeField] private EventReference m_endMermaid;
    [SerializeField] private EventReference m_endCorpo;
    [SerializeField] private EventReference m_afterEndCorpo;

    private readonly float[] m_mixFree = {
        // Input -> Mono ↓Speaker
        0, 0, 0, 0, 0, 0, // L
        0, 0, 0, 0, 0, 0, // C
        0, 0, 0, 0, 0, 0, // R
        0, 0, 0, 0, 0, 0, // LFE
        0, 0, 0, 0, 0, 0, // LS
        0, 0, 0, 0, 0, 0  // RS
    };

    private readonly float[] m_mixPrison = {
        // Input -> Mono ↓Speaker
        1, -1, -1, -1, -1, -1, // L
        0, 0, 0, 0, 0, 0,      // C
        0, 0, 0, 0, 0, 0,      // R
        0, 0, 0, 0, 0, 0,      // LFE
        0, 0, 0, 0, 0, 0,      // LS
        0, 0, 0, 0, 0, 0       // RS
    };

    private bool m_canSing;

    private ChannelGroup m_mermaidChannels;
    private float m_timer;

    protected new IEnumerator Start() {
        base.Start();
        Bus bus = RuntimeManager.GetBus(m_pathBusMermaid);
        while (bus.getChannelGroup(out m_mermaidChannels) != RESULT.OK) { yield return new WaitForEndOfFrame(); }
        Debug.Log("Fmod Bus loaded.");
        ChangeMermaidToPrison();
    }

    private void Update() {
        if (m_canSing) {
            if (m_timer < 0) {
                if (Random.value < 0.01f * m_vocalsProba) {
                    EventInstance eventInstance = GetEventInstance(m_sight);
                    eventInstance.start();
                    eventInstance.getDescription(out EventDescription description);
                    description.getLength(out int length);
                    m_timer = length + m_waitVocals;
                }
                m_timer = m_waitVocals;
            }
            else { m_timer -= Time.deltaTime; }
        }
        //if()
    }

    public void ChangeMermaidToPrison() { m_mermaidChannels.setMixLevelsOutput(0, 1, 0, 0, 0, 0, 0, 0); }

    public void ChangeMermaidToFree() { m_mermaidChannels.setMixLevelsOutput(0, 0, 1, 0, 0, 0, 0, 0); }

    #region Events AI
    public void OnResetAI(DelegateTimer timer) {
        ChangeMermaidToPrison();
        m_canSing = true;
        GetEventInstance(m_endCorpo).stop(STOP_MODE.IMMEDIATE);
        GetEventInstance(m_afterEndCorpo).stop(STOP_MODE.IMMEDIATE);
        GetEventInstance(m_endMermaid).stop(STOP_MODE.IMMEDIATE);
    }

    public void OnOpenCageAI(DelegateTimer timer) {
        m_canSing = false;
        GetEventInstance(m_sight).stop(STOP_MODE.ALLOWFADEOUT);
    }

    public void OnResetFreeAI(DelegateTimer timer) {
        m_canSing = false;
        ChangeMermaidToFree();
        GetEventInstance(m_endCorpo).stop(STOP_MODE.IMMEDIATE);
        GetEventInstance(m_afterEndCorpo).stop(STOP_MODE.IMMEDIATE);
        GetEventInstance(m_endMermaid).stop(STOP_MODE.IMMEDIATE);
    }

    private void OnShowNumberAI(DelegateTimer timer, int number) {
        EventInstance eventInstance = GetEventInstance(m_validCompting);
        eventInstance.start();
        /*Debug.Log(eventInstance.getDescription(out EventDescription description));
        Debug.Log(description.getLength(out int length));
        Debug.Log(length);*/
        timer(1.0f);
    }

    public void OnShow1AI(DelegateTimer timer) { OnShowNumberAI(timer, 1); }

    public void OnShow2AI(DelegateTimer timer) { OnShowNumberAI(timer, 2); }

    public void OnShow3AI(DelegateTimer timer) { OnShowNumberAI(timer, 3); }

    public void OnShow4AI(DelegateTimer timer) { OnShowNumberAI(timer, 4); }

    public void OnShow5AI(DelegateTimer timer) { OnShowNumberAI(timer, 5); }

    public void OnTreasureUnlockedAI(DelegateTimer timer) {
        ChangeMermaidToFree();
        GetEventInstance(m_endCorpo).stop(STOP_MODE.IMMEDIATE);
        GetEventInstance(m_afterEndCorpo).stop(STOP_MODE.IMMEDIATE);
        GetEventInstance(m_endMermaid).stop(STOP_MODE.IMMEDIATE);
    }
    #endregion

    #region Events Anims
    public void QuickLook() {
        m_canSing = false;
        GetEventInstance(m_gaspHeadTurn).start();
        m_timer = 2 * m_waitVocals;
    }

    public void QuickLookExit() { m_canSing = true; }

    public void Release() {
        m_canSing = false;
        GetEventInstance(m_freedomShot).start();
    }

    public void ReleaseExit() { ChangeMermaidToFree(); }

    public void Idle() { GetEventInstance(m_vocalise).start(); }

    public void IdleExit() { GetEventInstance(m_vocalise).stop(STOP_MODE.IMMEDIATE); }

    public void TreasureFound() { GetEventInstance(m_gaspSurprise).start(); }

    public void TreasureGet() { GetEventInstance(m_dilemma).start(); }

    public void Keep() {
        GetEventInstance(m_endCorpo).start();
        GetEventInstance(m_endMermaid).stop(STOP_MODE.IMMEDIATE);
        GetEventInstance(m_afterEndCorpo).stop(STOP_MODE.IMMEDIATE);
    }

    public void KeepExit() { GetEventInstance(m_afterEndCorpo).start(); }

    public void Give() {
        GetEventInstance(m_endMermaid).start();
        GetEventInstance(m_endCorpo).stop(STOP_MODE.IMMEDIATE);
    }
    #endregion

}