using FMODUnity;

using UnityEngine;

[DefaultExecutionOrder(-1)]
public class AudioGlobalSounds : AudioEmitterBase {
    [Header("Events")]
    [SerializeField] private EventReference m_ambiance;

    protected override void Start() {
        base.Start();
        GetEventInstance(m_ambiance).start();
    }

    public void OnMute(bool mute) { RuntimeManager.MuteAllEvents(mute); }
}