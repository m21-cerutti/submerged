
using FMODUnity;

using m21cerutti.ClapClapEvent.CustomEvents;

using UnityEngine;
using UnityEngine.Serialization;

public class AudioCage :  AudioEmitterBase {

    [Header("Events")]
    [SerializeField] private EventReference m_cage;


    public void OpenCageSound() { GetEventInstance(m_cage).start(); }
}
