using UnityEngine;

[DefaultExecutionOrder(400)]
public class HeadAimIK : MonoBehaviour {

    [SerializeField] private bool m_track;
    [SerializeField] private Transform m_target;

    [Header("Face & Eyes")]
    [SerializeField] private Transform m_face;
    [SerializeField] private Vector3 m_angleFaceOffset;
    [SerializeField] private float m_faceSpeed = 10;
    [SerializeField] private Transform[] m_eyes = new Transform[2];
    [SerializeField] private Vector3[] m_angleEyeOffsets = new Vector3[2];
    [SerializeField] private float[] m_eyeSpeeds = new float[2];

    [Header("Laterals")]
    [SerializeField] private Transform m_body;
    [SerializeField] private Transform m_exteriorCam;
    [SerializeField] private Vector2 m_angleRange;
    [SerializeField] private float m_displacementStrength;
    [SerializeField] private float m_speedSmoothDisplacement;
    [SerializeField] private float m_minMagnitudeMove;
    [SerializeField] private float m_timeMinMove;
    private readonly Quaternion[] m_rotationEyesOverride = new Quaternion[2];
    private Vector3 m_aimDisplacement;
    private float m_angle;
    private Vector3 m_currentTargetPosition;

    private Quaternion m_rotationFaceOverride;

    private float m_timerMove = -1;
    private Vector3 m_velocityDisplacement;

    private void Start() {
        m_currentTargetPosition = m_target.position;
        m_rotationFaceOverride = m_face.rotation;
        m_rotationEyesOverride[0] = m_eyes[0].rotation;
        m_rotationEyesOverride[1] = m_eyes[1].rotation;
    }

    private void Update() {
        // Move
        if (m_timerMove > 0) { m_timerMove -= Time.deltaTime; }
        else if (m_timerMove < 0) { m_timerMove = 0; }
    }

    private void LateUpdate() {
        if (m_target != null) {
            if (m_track) {
                MoveTowardTarget(m_currentTargetPosition);
                RotateTowardTarget(m_exteriorCam.position);

                // Body position timer
                if ((m_currentTargetPosition - m_target.position).magnitude > m_minMagnitudeMove) {
                    if (m_timerMove == 0) {
                        m_currentTargetPosition = m_target.position;
                        m_timerMove = m_timeMinMove;
                    }
                }
                // Face & eyes
                SmoothLookAt(ref m_rotationFaceOverride, ref m_face, m_angleFaceOffset, m_target, m_faceSpeed);
                SmoothLookAt(ref m_rotationEyesOverride[0],
                             ref m_eyes[0],
                             m_angleEyeOffsets[0],
                             m_target,
                             m_eyeSpeeds[0]);
                SmoothLookAt(ref m_rotationEyesOverride[1],
                             ref m_eyes[1],
                             m_angleEyeOffsets[1],
                             m_target,
                             m_eyeSpeeds[1]);
            }
            else {
                MoveTowardTarget(m_body.position + (m_body.forward * 5.0f));
                SmoothLookAt(ref m_rotationFaceOverride, ref m_face, m_face.rotation, m_faceSpeed);
                SmoothLookAt(ref m_rotationEyesOverride[0], ref m_eyes[0], m_eyes[0].rotation, m_eyeSpeeds[0]);
                SmoothLookAt(ref m_rotationEyesOverride[1], ref m_eyes[1], m_eyes[1].rotation, m_eyeSpeeds[1]);
            }
        }
    }

    private void RotateTowardTarget(Vector3 targetPos) {
        Vector3 distCam = (targetPos - m_body.position).normalized;
        distCam.y = 0;
        Quaternion lookRotation = Quaternion.LookRotation(distCam, Vector3.up);
        if (Quaternion.Angle(m_body.rotation, lookRotation) > 1.0f) {
            m_body.rotation =
                Quaternion.Slerp(m_body.rotation, lookRotation, 0.5f);
        }
    }

    private void MoveTowardTarget(Vector3 targetPos) {
        Vector3 position = m_body.position;
        Vector3 distPlayer = (targetPos - position).normalized;
        distPlayer.y = 0;
        m_angle = Vector3.SignedAngle(distPlayer, new Vector3(0, 0, 1), new Vector3(0, 1, 0));
        float leftRightMove = (m_angle - m_angleRange.x) / (m_angleRange.y - m_angleRange.x);
        // Get limit left-right
        leftRightMove = (Mathf.Clamp01(leftRightMove) * 2) - 1;
        Vector3 displacement = Vector3.left * (m_displacementStrength * leftRightMove);
        // Smooth
        m_aimDisplacement =
            Vector3.SmoothDamp(m_aimDisplacement,
                               displacement,
                               ref m_velocityDisplacement,
                               Time.deltaTime * m_speedSmoothDisplacement);
        position += m_aimDisplacement;
        m_body.position = position;
    }

    private void SmoothLookAt(ref Quaternion rotation, ref Transform element, Vector3 angleOffset, Transform target,
                              float speed) {
        SmoothLookAt(ref rotation,
                     ref element,
                     Quaternion.LookRotation(target.position - element.position, Vector3.up) *
                     Quaternion.Euler(angleOffset),
                     speed);
    }

    private void SmoothLookAt(ref Quaternion rotation, ref Transform element, Quaternion aim, float speed) {
        rotation =
            Quaternion.Slerp(rotation,
                             aim,
                             Time.deltaTime * speed);
        element.rotation = rotation;
    }

    public void SetTrack(bool track) {
        m_track = track;
        m_rotationFaceOverride = m_face.rotation;
        m_rotationEyesOverride[0] = m_eyes[0].rotation;
        m_rotationEyesOverride[1] = m_eyes[1].rotation;
    }
}