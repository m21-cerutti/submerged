using UnityEngine;

public class CustomTurn : StateMachineBehaviour {
    [SerializeField] private Vector3 m_eulerRotation;
    private Quaternion m_startRotation;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        m_startRotation = animator.transform.rotation;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.transform.rotation = m_startRotation * Quaternion.Euler(m_eulerRotation);
        // Custom
        animator.GetComponentInParent<MermaidController>().MoveHide();
    }
}