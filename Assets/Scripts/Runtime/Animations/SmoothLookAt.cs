using UnityEngine;

[DefaultExecutionOrder(400)]
public class SmoothLookAt : MonoBehaviour {
    [SerializeField] private GameObject m_target;
    [SerializeField] private float m_speed = 10;
    [SerializeField] private Vector3 m_eulerOffset;
    [SerializeField] private bool m_track;

    private Quaternion m_rotationOverride;

    private void Start() { m_rotationOverride = transform.rotation; }

    private void LateUpdate() {
        if (m_target != null && m_track) {
            m_rotationOverride =
                Quaternion.Slerp(m_rotationOverride,
                                 Quaternion.LookRotation(
                                     m_target.transform.position - transform.position,
                                     Vector3.up)
                                 * Quaternion.Euler(m_eulerOffset),
                                 Time.deltaTime * m_speed);

            transform.rotation = m_rotationOverride;
        }
        else if (m_target != null) {
            Quaternion animRot = transform.rotation;
            m_rotationOverride =
                Quaternion.Slerp(m_rotationOverride,
                                 animRot,
                                 Time.deltaTime * m_speed);
            transform.rotation = m_rotationOverride;
        }
    }

    public void SetTrack(bool track) {
        m_track = track;
        m_rotationOverride = transform.rotation;
    }
}