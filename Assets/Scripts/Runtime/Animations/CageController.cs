using m21cerutti.ClapClapEvent.CustomEvents;

using UnityEngine;
#if UNITY_EDITOR
using System.Collections.Generic;

using UnityEditor.Animations;
#endif

public class CageController : MonoBehaviour {
    [SerializeField] private LengthClip[] m_animsLength;

    private readonly int m_freeTrigger = Animator.StringToHash("Free");
    private readonly int m_resetDilemmaTrigger = Animator.StringToHash("ResetDilemma");
    private readonly int m_resetFreeTrigger = Animator.StringToHash("ResetFree");
    private readonly int m_resetTrigger = Animator.StringToHash("Reset");
    private Animator m_animatorCage;

    private void Start() { m_animatorCage = GetComponentInChildren<Animator>(); }

#if UNITY_EDITOR
    [ContextMenu("Populate animations")]
    private void PopulateAnimationsClips() {
        Start();
        AnimatorController controller = m_animatorCage.runtimeAnimatorController as AnimatorController;
        List<LengthClip> clips = new List<LengthClip>();
        int i = 0;
        foreach (ChildAnimatorState stateAnim in controller.layers[0].stateMachine.states) {
            if (!stateAnim.state.motion.isLooping) {
                clips.Add(new LengthClip($"{i}_{stateAnim.state.name}",
                                         stateAnim.state.motion.averageDuration));
                i++;
            }
        }
        m_animsLength = clips.ToArray();
    }
#endif

    #region Events
    public void OnResetAI(DelegateTimer timer) {
        m_animatorCage.ResetAllAnimatorParameters();
        m_animatorCage.SetTrigger(m_resetTrigger);
    }

    public void OnOpenCageAI(DelegateTimer timer) {
        m_animatorCage.SetTrigger(m_freeTrigger);
        timer(m_animsLength[0].m_length);
    }

    public void OnResetFreeAI(DelegateTimer timer) {
        m_animatorCage.ResetAllAnimatorParameters();
        m_animatorCage.SetTrigger(m_resetFreeTrigger);
    }

    public void OnTreasureUnlockedAI(DelegateTimer timer) {
        m_animatorCage.ResetAllAnimatorParameters();
        m_animatorCage.SetTrigger(m_resetDilemmaTrigger);
    }
    #endregion

}