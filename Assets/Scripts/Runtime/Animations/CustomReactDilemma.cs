using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomReactDilemma : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponentInParent<MermaidController>().DeactivateHeadTrack();
    }
    
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponentInParent<MermaidController>().ActivateHeadTrack();
    }
}
