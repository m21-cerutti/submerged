using UnityEngine;

public class CustomFree : StateMachineBehaviour {

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.GetComponentInParent<MermaidController>().MoveFree();
    }
}