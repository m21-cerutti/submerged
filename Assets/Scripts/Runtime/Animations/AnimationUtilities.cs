using UnityEngine;

public static class AnimationUtilities {
    public static void ResetAllAnimatorParameters(this Animator animator) {
        foreach (AnimatorControllerParameter parameter in animator.parameters) {
            switch (parameter.type) {
                case AnimatorControllerParameterType.Float:
                    animator.SetFloat(parameter.name, 0.0f);

                    break;

                case AnimatorControllerParameterType.Int:
                    animator.SetInteger(parameter.name, 0);

                    break;

                case AnimatorControllerParameterType.Bool:
                    animator.SetBool(parameter.name, false);

                    break;

                case AnimatorControllerParameterType.Trigger:
                    animator.ResetTrigger(parameter.name);

                    break;
            }
        }
    }
}