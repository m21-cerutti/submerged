using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomIdleMermaid : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<AudioMermaid>().Idle();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<AudioMermaid>().IdleExit();
    }
}
