using System;
using System.Collections;
using System.IO.Ports;

using m21cerutti.ClapClapEvent.CustomEvents;

using UnityEngine;

public enum ModeCard {
    READ,
    REGISTER_FILE
}

public class ArduinoSerial : MonoBehaviour {

    [SerializeField] private PhysicObjectEvent m_event;

    [Header("Card mode")] [SerializeField] private ModeCard m_mode;
    private readonly int m_timeRetry = 1;

    private bool m_isInit;
    private int m_nbRetry = 2;
    private SerialPort m_serial;

    private void Awake() {
        m_mode = ModeCard.READ;
        DisplayPorts();
    }

    private IEnumerator Start() {
        ArduinoConfigLoader.instance.Load();
        m_serial = new SerialPort();
        m_serial.PortName = ArduinoConfigLoader.instance.Config.m_portName;
        m_serial.Parity = Parity.None;
        m_serial.BaudRate = 115200;
        m_serial.DataBits = 8;
        m_serial.StopBits = StopBits.One;
        try { m_serial.Open(); }
        catch (Exception e) {
            HardwareDebugger.instance.HDebugError($"Can't initialised. {e.Message}", Hardware.Arduino);

            yield break;
        }
        while (!m_isInit) { yield return Init(); }
    }

    // Update is called once per frame
    private void Update() {
        if (m_isInit && m_serial.BytesToRead > 0) {
            string text = m_serial.ReadLine();
            SwitchCommand(text);
        }
    }

    private void OnApplicationQuit() {
        m_serial.Close();
        ArduinoConfigLoader.instance.Save();
    }

    private IEnumerator Init() {
        if (m_nbRetry == 0 || !m_serial.IsOpen) {
            HardwareDebugger.instance.HDebugError("Can't initialised.", Hardware.Arduino);
            m_nbRetry--;
        }
        else if (m_serial.BytesToRead > 0) {
            string text = m_serial.ReadLine();
            if (text == "C") {
                HardwareDebugger.instance.HDebug("Card init", Hardware.Arduino);
                m_isInit = true;
            }
            else { HardwareDebugger.instance.HDebugError($"{text} not command expected", Hardware.Arduino); }
        }
        else {
            m_serial.Write("C");
            m_nbRetry--;
        }

        yield return new WaitForSeconds(m_timeRetry);
    }

    private void OnRegisterArduinoModeChanged(bool value) { m_mode = value ? ModeCard.REGISTER_FILE : ModeCard.READ; }

    private void OnTestConnection() { m_serial.Write("C"); }

    public void DisplayPorts() {
        // Get port names
        string[] portNames = SerialPort.GetPortNames();
        HardwareDebugger.instance.HDebug($"PORTS: {string.Join(';', portNames)}", Hardware.Arduino);
    }

    private void SwitchCommand(string cmd) {
        if (cmd.StartsWith("ERROR")) { HardwareDebugger.instance.HDebugError(cmd, Hardware.Arduino); }
        else if (cmd.StartsWith("D")) {
            string uid = cmd.Split(':')[1];
            ObjectDetected(uid);
        }
        else if (cmd.StartsWith("R")) {
            string uid = cmd.Split(':')[1];
            ObjectRemoved(uid);
        }
        else { HardwareDebugger.instance.HDebug($"{cmd} (from device)", Hardware.Arduino); }
    }

    private void ObjectDetected(string uid) {
        switch (m_mode) {
            case ModeCard.REGISTER_FILE:
                bool r = ArduinoConfigLoader.instance.uidsKeys.TryAdd(uid, PhysicObject.None);
                HardwareDebugger.instance.HDebug(
                    $"REGISTER: {uid} {(r ? "success" : $"already exist -> {ArduinoConfigLoader.instance.uidsKeys[uid]}")}",
                    Hardware.Arduino);
                if (r) { ArduinoConfigLoader.instance.Save(true); }

                break;

            case ModeCard.READ:
            default:
                if (!ArduinoConfigLoader.instance.uidsKeys.ContainsKey(uid)) {
                    HardwareDebugger.instance.HDebugWarn($"{uid} not in config.", Hardware.Arduino);

                    return;
                }
                PhysicObject obj = ArduinoConfigLoader.instance.uidsKeys[uid];
                if (obj == PhysicObject.None) {
                    HardwareDebugger.instance.HDebugWarn($"{uid} not set in object list", Hardware.Arduino);

                    return;
                }
                m_event.Invoke(obj);

                break;
        }
    }

    private void ObjectRemoved(string uid) {
        switch (m_mode) {
            case ModeCard.REGISTER_FILE:
                HardwareDebugger.instance.HDebug($"{uid} removed", Hardware.Arduino);

                break;

            case ModeCard.READ:
            default:
                if (!ArduinoConfigLoader.instance.uidsKeys.ContainsKey(uid)) {
                    HardwareDebugger.instance.HDebugWarn($"{uid} not in config.", Hardware.Arduino);

                    return;
                }
                PhysicObject obj = ArduinoConfigLoader.instance.uidsKeys[uid];
                if (obj == PhysicObject.None) { return; }
                m_event.Invoke(PhysicObject.None);

                break;
        }
    }
}