using System;
using System.Collections.Generic;

[Serializable]
public struct UidTranslate {
    public string m_uid;
    public string m_key;

    public UidTranslate(string uid, string key) {
        m_uid = uid;
        m_key = key;
    }
}

[Serializable]
public class ArduinoConfig {
    public string m_portName;
    public List<UidTranslate> m_uidsSerialize = new List<UidTranslate>();
}