using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;

[ExecuteInEditMode]
public class ArduinoConfigLoader : ConfigLoader<ArduinoConfig, ArduinoConfigLoader> {

    // Name of scene config file.
    private const string ARDUINO_DATA_FILE_NAME = "arduino.json";

    public Dictionary<string, PhysicObject> uidsKeys = new Dictionary<string, PhysicObject>();

    public override void Save(bool overwrite = false) {
        if (!overwrite && File.Exists(GetPathConfig())) { return; }
        Config.m_uidsSerialize = uidsKeys.Select(x => new UidTranslate(x.Key, x.Value.ToString())).ToList();
        string data = JsonUtility.ToJson(Config, true);
        File.WriteAllText(GetPathConfig(), data);
    }

    [ContextMenu("Reload config")]
    public override void Load() {
        if (File.Exists(GetPathConfig())) {
            string dataAsJson = File.ReadAllText(GetPathConfig());
            Config = JsonUtility.FromJson<ArduinoConfig>(dataAsJson);
            uidsKeys = Config.m_uidsSerialize.ToDictionary(x => x.m_uid, x => Enum.Parse<PhysicObject>(x.m_key));
            Debug.Log("Successfully loaded arduino config file.");
        }
        else { Debug.LogError("Cannot load arduino data!"); }
    }

    public override string GetPathConfig() {
        return Path.Combine(Application.streamingAssetsPath, ARDUINO_DATA_FILE_NAME);
    }
}