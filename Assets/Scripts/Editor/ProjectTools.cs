using System.Collections;
using System.IO;

using m21cerutti.BUS.Editor;

using Unity.EditorCoroutines.Editor;

using UnityEditor;

using UnityEngine;

public class ProjectTools : EditorWindow {

    private const string PATH_BEHAVIOUR_CONTAINER = "Assets/Data/BehaviourContainer.asset";
    private const string PATH_OBJECTS_BANK = "Assets/Data/ObjectsBank.asset";

    [SerializeField] private int m_sizeHeaderLines = 4;

    private void OnGUI() {
        GUI.enabled = !EditorApplication.isPlayingOrWillChangePlaymode || EditorApplication.isPaused;

        GUILayout.Label("BehaviorTable");
        m_sizeHeaderLines = EditorGUILayout.IntField("NbLines Header", m_sizeHeaderLines);
        FuncEditor.DrawUILine(Color.gray);

        if (GUILayout.Button("Sync with drive")) { Load(); }
        if (GUILayout.Button("Sync local")) { LoadLocal(); }
        if (GUILayout.Button("Open folder backup")) { Application.OpenURL(DriveAiDownloader.GetPathFolderBackup()); }
        if (GUILayout.Button("Open arduino config")) {
            Application.OpenURL(ArduinoConfigLoader.instance.GetPathConfig());
        }
        if (GUILayout.Button("Open kinect config")) {
            Application.OpenURL(KinectConfigLoader.instance.GetPathConfig());
        }
        if (GUILayout.Button("Open monitor config")) {
            Debug.Log(MonitorConfigLoader.instance);
            Application.OpenURL(MonitorConfigLoader.instance.GetPathConfig());
        }
        EditorGUILayout.Separator();
        GUI.enabled = true;
    }

    [MenuItem("Tools/ProjectTools")] public static void ShowWindow() { GetWindow(typeof(ProjectTools)); }

    private void Load() {
        EditorCoroutineUtility.StartCoroutine(
            DriveAiDownloader.DownloadAllData(AfterDownloadEnum, AfterDownloadBehaviorTable),
            this);
    }

    private void LoadLocal() {
        string path_enum = DriveAiDownloader.GetPathFolderBackup() + DriveAiDownloader.fileEnum;
        if (!File.Exists(path_enum)) {
            Debug.LogError($"No existing backup {path_enum}");

            return;
        }
        EditorCoroutineUtility.StartCoroutine(ProcessEnum(path_enum), this);

        string path_behaviorTable = DriveAiDownloader.GetPathFolderBackup() + DriveAiDownloader.fileBehaviourTable;
        if (!File.Exists(path_behaviorTable)) {
            Debug.LogError($"No existing backup {path_behaviorTable}");

            return;
        }
        EditorCoroutineUtility.StartCoroutine(ProcessBehaviorTable(path_behaviorTable), this);
    }

    private void AfterDownloadEnum(string error) {
        string path = DriveAiDownloader.GetPathFolderBackup() + DriveAiDownloader.fileEnum;
        if (error != null) { Debug.LogError(error); }
        if (!File.Exists(path)) {
            Debug.LogError($"No existing backup {path}");

            return;
        }
        EditorCoroutineUtility.StartCoroutine(ProcessEnum(path), this);
    }

    private void AfterDownloadBehaviorTable(string error) {
        string path = DriveAiDownloader.GetPathFolderBackup() + DriveAiDownloader.fileBehaviourTable;
        if (error != null) { Debug.LogError(error); }
        if (!File.Exists(path)) {
            Debug.LogError($"No existing backup {path}");

            return;
        }

        EditorCoroutineUtility.StartCoroutine(ProcessBehaviorTable(path), this);
    }

    private IEnumerator ProcessEnum(string path) {
        yield return new WaitForEndOfFrame();
        string error = CsvEnumsConverter.ProcessFile(path);
        if (error != null) {
            Debug.LogError(error);

            yield break;
        }
        AssetDatabase.Refresh();
        Debug.Log("Enum sync done.");
    }

    private IEnumerator ProcessBehaviorTable(string path) {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        AssetDatabase.Refresh();
        BehaviourContainer container = AssetDatabase.LoadAssetAtPath<BehaviourContainer>(PATH_BEHAVIOUR_CONTAINER);
        string error = CsvBehaviourTableConverter.ProcessFile(path, m_sizeHeaderLines, container);
        if (error != null) {
            Debug.LogError(error);

            yield break;
        }

        Debug.Log("BehaviorTable sync done.");
    }
}