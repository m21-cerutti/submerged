using System.IO;
using System.Text.RegularExpressions;

public static class CsvEnumsConverter {

    private const string PATH_ENUM_SCRIPTS = "Assets/Scripts/Runtime/Enums/";

    public static string ProcessFile(string path) {
        StreamWriter[] enumsFile = null;
        using (StreamReader reader = new StreamReader(path)) {
            string line;
            int l = -1;
            while ((line = reader.ReadLine()) != null) {
                l++;
                //Define pattern
                Regex csvParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                //Separating columns to array
                string[] columns = csvParser.Split(line);

                if (l == 0) {
                    CreateFilesEnum(columns, out enumsFile);

                    continue;
                }

                AppendEnumName(enumsFile, columns, l);
            }
            foreach (StreamWriter f in enumsFile) {
                f.WriteLine("}");
                f.Close();
            }
        }

        return null;
    }

    private static void CreateFilesEnum(string[] names, out StreamWriter[] enums) {
        enums = new StreamWriter[names.Length];
        int i = 0;
        foreach (string n in names) {
            if (!string.IsNullOrEmpty(n)) {
                enums[i] = File.CreateText(PATH_ENUM_SCRIPTS + n + ".cs");

                enums[i].WriteLine($"public enum {n} {{");
            }
            i++;
        }
    }

    private static void AppendEnumName(StreamWriter[] enumsFile, string[] columns, int indexLine) {
        int i = 0;
        foreach (StreamWriter f in enumsFile) {
            if (!string.IsNullOrEmpty(columns[i])) { f.WriteLine($"\t\t{columns[i]},"); }
            i++;
        }
    }
}