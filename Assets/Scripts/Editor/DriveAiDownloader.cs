using System;
using System.Collections;
using System.IO;

using UnityEngine;
using UnityEngine.Networking;

public static class DriveAiDownloader {
    // Access drive
    private const string K_GOOGLE_SHEET_DOC_ID = "1hdZVV440MIvvsWpWe88pXxTgoEoGAUbiajHkAbJu5zc";
    private const string K_ENUM_GUID = "1830862591";
    private const string URL_ENUMS = "https://docs.google.com/spreadsheets/d/" + K_GOOGLE_SHEET_DOC_ID +
                                     "/export?format=csv&gid=" + K_ENUM_GUID;
    //https://docs.google.com/spreadsheets/d/1hdZVV440MIvvsWpWe88pXxTgoEoGAUbiajHkAbJu5zc/export?format=csv&gid=1830862591
    private const string URL_BEHAVIOUR_TABLE =
        "https://docs.google.com/spreadsheets/d/" + K_GOOGLE_SHEET_DOC_ID + "/export?format=csv";
    //https://docs.google.com/spreadsheets/d/1hdZVV440MIvvsWpWe88pXxTgoEoGAUbiajHkAbJu5zc/export?format=csv
    private const string PATH_FOLDER_BACKUP = "../backup/";

    // Backup files
    public const string fileEnum = "enums.csv";
    public const string fileBehaviourTable = "ai.csv";

    public static string GetPathFolderBackup() { return $"{Application.dataPath}/{PATH_FOLDER_BACKUP}/"; }

    public static IEnumerator DownloadAllData(Action<string> onCompletedEnum,
                                              Action<string> onCompletedBehaviourTable) {
        yield return DownloadData(URL_ENUMS, fileEnum, onCompletedEnum);
        yield return DownloadData(URL_BEHAVIOUR_TABLE, fileBehaviourTable, onCompletedBehaviourTable);
    }

    private static IEnumerator DownloadData(string url, string nameBackup, Action<string> finish) {
        string errorMessage = null;
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url)) {
            Debug.Log($"Download {nameBackup}...");

            yield return webRequest.SendWebRequest();
            if (webRequest.result == UnityWebRequest.Result.Success) {
                string downloadData = webRequest.downloadHandler.text;
                Backup(nameBackup, downloadData);
            }
            else { errorMessage = $"Download {nameBackup} error {url}: {webRequest.error}"; }
        }
        finish(errorMessage);
    }

    private static void Backup(string filename, string data) {
        string path = GetPathFolderBackup() + filename;
        File.WriteAllText(path, data);
    }
}