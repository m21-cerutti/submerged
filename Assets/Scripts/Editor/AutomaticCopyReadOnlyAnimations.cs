﻿using System.IO;

using UnityEditor;

using UnityEngine;

[InitializeOnLoad]
public class AutomaticCopyReadOnlyAnimations : AssetPostprocessor {
    private readonly string pathAnimation = "Assets/Data/Animation";

    public void OnPostprocessAnimation(GameObject g, AnimationClip clip) {
        AnimationClip newAnim = new AnimationClip();
        EditorUtility.CopySerialized(clip, newAnim);
        newAnim.name = clip.name.Replace("|", "_");
        string path = $"{pathAnimation}/{g.name}/{newAnim.name}.anim";
        if (!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), pathAnimation, g.name))) {
            AssetDatabase.CreateFolder(pathAnimation, g.name);
            AssetDatabase.Refresh();
        }
        AnimationClip old = AssetDatabase.LoadAssetAtPath<AnimationClip>(path);
        if (old != null) {
            AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(newAnim);
            settings.loopTime = old.isLooping;
            AnimationUtility.SetAnimationClipSettings(newAnim, settings);
            AnimationUtility.SetAnimationEvents(newAnim, old.events);
            EditorUtility.CopySerialized(newAnim, old);
            EditorUtility.SetDirty(old);
        }
        else { AssetDatabase.CreateAsset(newAnim, path); }
        AssetDatabase.SaveAssets();
    }
}