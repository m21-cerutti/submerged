using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using m21cerutti.BUS.Runtime;
using m21cerutti.ClapClapEvent.CustomEvents;
using m21cerutti.ClapClapEvent.Editor;

using UnityEditor;

using UnityEngine;

public static class CsvBehaviourTableConverter {

    public static string ProcessFile(string path, int nbHeaderLines, BehaviourContainer container) {
        List<AiBehaviour> behaviours = new List<AiBehaviour>();
        using (StreamReader reader = new StreamReader(path)) {
            string line;

            int i = -1;
            while ((line = reader.ReadLine()) != null) {
                i++;
                if (i < nbHeaderLines) { continue; }
                //Define pattern
                Regex csvParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                //Separating columns to array
                string[] columns = csvParser.Split(line);

                // Parse csv line to AiBehaviour
                AiBehaviour b = ProcessLineFromCsv(columns, i);
                behaviours.Add(b);
            }
            container.FillContainer(behaviours);
        }

        return null;
    }

    private static AiBehaviour ProcessLineFromCsv(string[] columns, int indexLine) {
        int indC = 0;

        // Id
        int id = int.Parse(columns[indC]);
        indC++;

        // Name
        string nameBehaviour = columns[indC];
        indC++;

        // Override
        KeyCode key = KeyCode.None;
        if (!string.IsNullOrEmpty(columns[indC])) {
            string keystr = columns[indC];
            if (char.IsDigit(keystr[0])) { keystr = $"Alpha{keystr}"; }
            key = Enum.Parse<KeyCode>(keystr);
        }
        indC++;

        AiBehaviour b = new AiBehaviour(id, key, nameBehaviour);

        // Observation (pass)
        indC++;

        //// Conditions

        bool stateCSet = false;
        // State
        if (!string.IsNullOrEmpty(columns[indC])) {
            StateGame state = Enum.Parse<StateGame>(columns[indC]);
            StateCondition sc = new StateCondition($"is{state.ToString()}State", state);
            b.AddCondition(sc);
            stateCSet = true;
        }
        indC++;

        // Time since last state
        if (!string.IsNullOrEmpty(columns[indC])) {
            string[] command = columns[indC].Split(' ');
            Comparer c = Enum.Parse<Comparer>(command[0]);
            int value = int.Parse(command[1]);
            TimeSinceLastStateCondition time =
                new TimeSinceLastStateCondition($"Time{c.ToString().FirstCharToUpper()}{value}", c, value);
            b.AddCondition(time);
        }
        indC++;

        // Object
        if (!string.IsNullOrEmpty(columns[indC])) {
            PhysicObject obj = Enum.Parse<PhysicObject>(columns[indC]);
            // Detection
            Detection detect = Detection.OnDeck;
            if (!string.IsNullOrEmpty(columns[indC + 1])) { detect = Enum.Parse<Detection>(columns[indC + 1]); }

            ObjectDetectionCondition odc = new ObjectDetectionCondition($"PO{obj.ToString()}", obj, detect);
            b.AddCondition(odc);
        }
        indC += 2;

        //// Actions

        // Event
        if (!string.IsNullOrEmpty(columns[indC])) {
            StringBuilder name = new StringBuilder();
            foreach (string w in columns[indC].Split(' ')) { name.Append(w.ToLower().FirstCharToUpper()); }
            string nameEvent = $"{name}AIEvent";
            DelegateTimerEvent ev;
            if (!ClapClapEditorUtilities.GetEventClapClapAsset(nameEvent, out ev)) {
                ev = ScriptableObject.CreateInstance(typeof(DelegateTimerEvent)) as DelegateTimerEvent;
                string assetPath =
                    AssetDatabase.GenerateUniqueAssetPath(
                        ClapClapEditorUtilities.GetDefaultEventFolderPath().Replace(Application.dataPath, "Assets")
                        + "/AIEvents/" + nameEvent + ".asset");
                AssetDatabase.CreateAsset(ev, assetPath);
                AssetDatabase.SaveAssets();
            }
            EventAction sa = new EventAction($"{name}AiEvent", ev);
            b.AddAction(sa);
        }
        indC++;

        bool stateChanged = false;
        // State change
        if (!string.IsNullOrEmpty(columns[indC])) {
            StateGame state = Enum.Parse<StateGame>(columns[indC]);
            StateChangeAction sca = new StateChangeAction($"StateTo{state}", state);
            b.AddAction(sca);
            stateChanged = true;
        }
        indC++;

        return b;
    }
}