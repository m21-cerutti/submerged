using NUnit.Framework;

using UnityEngine;

public class BasicEditorTest {
    [OneTimeSetUp] public void Init() {}

    [SetUp] public void BeforeTest() {}

    [TearDown] public void AfterTest() {}

    [OneTimeTearDown] public void End() {}

    [Test] public void ArduinoConfigValidation() {
        ArduinoConfigLoader config = Object.FindObjectOfType<ArduinoConfigLoader>();
        if (config == null) {
            GameObject g = new GameObject();
            config = g.AddComponent<ArduinoConfigLoader>();
        }
        config.Load();
        config.Save();
    }
}