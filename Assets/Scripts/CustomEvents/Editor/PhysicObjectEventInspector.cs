namespace m21cerutti.ClapClapEvent.CustomEvents.Editor {
    using UnityEditor;

    using UnityEngine;

    [CustomEditor(typeof(PhysicObjectEvent), true)] [CanEditMultipleObjects]
    public class PhysicObjectEventInspector : Editor {
        private PhysicObjectEvent[] m_objs;
        private PhysicObject m_parameter;

        public void OnEnable() {
            //Multi object management
            Object[] target_cast = targets;
            m_objs = new PhysicObjectEvent[targets.Length];
            int i = 0;
            foreach (Object o in targets) {
                m_objs[i] = (PhysicObjectEvent) o;
                i++;
            }
        }

        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            m_parameter = (PhysicObject) EditorGUILayout.EnumPopup("Parameter :", m_parameter);
            EditorGUILayout.Separator();
            if (GUILayout.Button("Invoke event")) {
                foreach (PhysicObjectEvent e in m_objs) { e.Invoke(m_parameter); }
            }
        }
    }
}