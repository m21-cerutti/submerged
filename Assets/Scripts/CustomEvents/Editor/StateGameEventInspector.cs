namespace m21cerutti.ClapClapEvent.CustomEvents.Editor {
    using UnityEditor;

    using UnityEngine;

    [CustomEditor(typeof(StateGameEvent), true)] [CanEditMultipleObjects]
    public class StateGameEventInspector : Editor {
        private StateGameEvent[] m_objs;
        private StateGame m_parameter;

        public void OnEnable() {
            //Multi object management
            Object[] target_cast = targets;
            m_objs = new StateGameEvent[targets.Length];
            int i = 0;
            foreach (Object o in targets) {
                m_objs[i] = (StateGameEvent) o;
                i++;
            }
        }

        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            m_parameter = (StateGame) EditorGUILayout.EnumPopup("Parameter :", m_parameter);
            EditorGUILayout.Separator();
            if (GUILayout.Button("Invoke event")) {
                foreach (StateGameEvent e in m_objs) { e.Invoke(m_parameter); }
            }
        }
    }
}