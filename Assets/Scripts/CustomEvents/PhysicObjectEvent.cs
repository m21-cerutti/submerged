namespace m21cerutti.ClapClapEvent.CustomEvents {
    using Runtime;

    using UnityEngine;

    [CreateAssetMenu(fileName = "PhysicObjectEvent", menuName = "Events/PhysicObjectEvent")]
    public class PhysicObjectEvent : ParametrisedEvent<PhysicObject> {}
}