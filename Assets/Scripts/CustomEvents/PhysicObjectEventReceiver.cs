namespace m21cerutti.ClapClapEvent.CustomEvents {
    using Runtime;

    public class PhysicObjectEventReceiver : MultiParamEventReceiver<PhysicObject> {}
}