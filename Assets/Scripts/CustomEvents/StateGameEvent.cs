namespace m21cerutti.ClapClapEvent.CustomEvents {
    using Runtime;

    using UnityEngine;

    [CreateAssetMenu(fileName = "StateGameEvent", menuName = "Events/StateGameEvent")]
    public class StateGameEvent : ParametrisedEvent<StateGame> {}
}