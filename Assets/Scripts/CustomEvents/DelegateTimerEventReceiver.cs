namespace m21cerutti.ClapClapEvent.CustomEvents {
    using Runtime;

    public class DelegateTimerEventReceiver : MultiParamEventReceiver<DelegateTimer> {}
}