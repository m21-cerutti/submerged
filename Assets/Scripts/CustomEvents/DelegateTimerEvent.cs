namespace m21cerutti.ClapClapEvent.CustomEvents {
    using Runtime;

    using UnityEngine;

    public delegate void DelegateTimer(float timer);

    [CreateAssetMenu(fileName = "DelegateFloatEvent", menuName = "Events/DelegateFloatEvent")]
    public class DelegateTimerEvent : ParametrisedEvent<DelegateTimer> {}
}