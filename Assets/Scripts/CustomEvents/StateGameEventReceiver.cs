namespace m21cerutti.ClapClapEvent.CustomEvents {
    using Runtime;

    public class StateGameEventReceiver : MultiParamEventReceiver<StateGame> {}
}