﻿namespace m21cerutti.ClapClapEvent.Editor {
	using System;
	using System.Reflection;

	using Runtime;

	using UnityEditor;

	using UnityEngine;

	public static class ClapClapEditorUtilities {

		private static readonly string defaultFolderEvents = "ProjectEvents";
		public static string GetDefaultEventFolderPath() { return $"{Application.dataPath}/{defaultFolderEvents}"; }

		public static bool GetEventClapClapAsset<T>(string fullname, out T ev)
			where T : EventClapClap {
			ev = null;
			string[] assets = AssetDatabase.FindAssets($"{fullname} t:EventClapClap");
			if (assets.Length == 0) {
				return false;
			}
			ev = AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(assets[0]));

			return ev != null;
		}

		// ! Not safe code ! Don't change function names
		public static void CallAddEventToListenerUnsafe(EventClapClap ev, GameObject g) {
			Type event_type = ev.GetType();
			Type receiver_type = Type.GetType(event_type.FullName + "Receiver, " + event_type.Assembly, true);
			Component c = g.GetComponent(receiver_type);
			if (c == null) {
				c = Undo.AddComponent(g, receiver_type);
			}
			MethodInfo method = receiver_type.GetMethod("AddPersistentEventHandler");
			method.Invoke(c, new object[] {ev});
		}
	}
}