namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	public class IntGameObjectEventReceiver : MultiParamEventReceiver<IntGameObject> { }
}