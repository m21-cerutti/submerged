namespace m21cerutti.ClapClapEvent.CustomEvents.Editor {
	using UnityEditor;

	using UnityEngine;

	[CustomEditor(typeof(PairStringBoolEvent), true), CanEditMultipleObjects]
	public class PairStringBoolEventInspector : Editor {
		private PairStringBoolEvent[] m_objs;
		private PairStringBool m_parameter;

		public void OnEnable() {
			//Multi object management
			Object[] target_cast = targets;
			m_objs = new PairStringBoolEvent[targets.Length];
			int i = 0;
			foreach (Object o in targets) {
				m_objs[i] = (PairStringBoolEvent) o;
				i++;
			}
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();
			// TODO Fill it
			//m_parameter = EditorGUILayout.PairStringBoolField("Parameter :", m_parameter); 
			EditorGUILayout.Separator();
			if (GUILayout.Button("Invoke event")) {
				foreach (PairStringBoolEvent e in m_objs) {
					e.Invoke(m_parameter);
				}
			}
		}
	}
}