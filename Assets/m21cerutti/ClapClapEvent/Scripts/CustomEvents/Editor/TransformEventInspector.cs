namespace m21cerutti.ClapClapEvent.CustomEvents.Editor {
	using UnityEditor;

	using UnityEngine;

	[CustomEditor(typeof(TransformEvent), true), CanEditMultipleObjects]
	public class TransformEventInspector : Editor {
		private TransformEvent[] m_objs;
		private Transform m_parameter;

		public void OnEnable() {
			//Multi object management
			Object[] target_cast = targets;
			m_objs = new TransformEvent[targets.Length];
			int i = 0;
			foreach (Object o in targets) {
				m_objs[i] = (TransformEvent) o;
				i++;
			}
		}

		public override void OnInspectorGUI() {
			DrawDefaultInspector();
			// TODO Fill it
			m_parameter = (Transform) EditorGUILayout.ObjectField("Parameter :", m_parameter, typeof(Transform), true);
			EditorGUILayout.Separator();
			if (GUILayout.Button("Invoke event")) {
				foreach (TransformEvent e in m_objs) {
					e.Invoke(m_parameter);
				}
			}
		}
	}
}