namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;
	
	using UnityEngine;
	
	public class TransformEventReceiver : MultiParamEventReceiver<Transform> { }
}