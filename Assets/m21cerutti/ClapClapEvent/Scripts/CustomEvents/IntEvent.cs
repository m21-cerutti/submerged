namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	[CreateAssetMenu(fileName = "IntEvent", menuName = "Events/IntEvent")]
	public class IntEvent : ParametrisedEvent<int> { }
}