namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;
	
	public struct PairStringBool {
		public string first;
		public bool second;
	}

	[CreateAssetMenu(fileName = "PairStringBoolEvent", menuName = "Events/PairStringBoolEvent")]
	public class PairStringBoolEvent : ParametrisedEvent<PairStringBool> { }
}