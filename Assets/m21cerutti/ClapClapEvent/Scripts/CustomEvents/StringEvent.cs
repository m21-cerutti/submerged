﻿namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	public delegate void StringEventCallback(string param);

	[CreateAssetMenu(fileName = "StringEvent", menuName = "Events/StringEvent")]
	public class StringEvent : ParametrisedEvent<string> { }
}