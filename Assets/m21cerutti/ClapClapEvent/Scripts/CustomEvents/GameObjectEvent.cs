﻿namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	public delegate void GameObjectEventCallback(GameObject param);

	[CreateAssetMenu(fileName = "GameObjectEvent", menuName = "Events/GameObjectEvent")]
	public class GameObjectEvent : ParametrisedEvent<GameObject> { }
}