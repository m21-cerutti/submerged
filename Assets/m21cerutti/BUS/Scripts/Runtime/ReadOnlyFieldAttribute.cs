namespace m21cerutti.BUS.Runtime {
	using UnityEngine;

	public class ReadOnlyFieldAttribute : PropertyAttribute { }
}