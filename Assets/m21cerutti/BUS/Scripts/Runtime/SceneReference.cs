namespace m21cerutti.BUS.Runtime {
	using System;

	using UnityEngine;
	using UnityEngine.SceneManagement;
	
#if UNITY_EDITOR
	using UnityEditor;
#endif

	// Based on JohannesMP (2018-08-12), 2019 S. Tarık Çetin, Tymski, MIT license
	//
	// Internally we serialize an Object to the SceneAsset which only exists at editor time.
	// Any time the object is serialized, we store the path provided by this Asset (assuming it was valid).
	//
	// This means that, come build time, the string path of the scene asset is always already stored, which if 
	// the scene was added to the build settings means it can be loaded.
	//
	// It is up to the user to ensure the scene exists in the build settings so it is loadable at runtime.
	// To help with this, a custom PropertyDrawer displays the scene build settings state.
	//
	//  Known issues:
	// - When reverting back to a prefab which has the asset stored as null, Unity will show the property 
	// as modified despite having just reverted. This only happens on the fist time, and reverting again fix it. 
	// Under the hood the state is still always valid and serialized correctly regardless.

	/// <summary>
	///     A wrapper that provides the means to safely serialize Scene Asset References.
	/// </summary>
	[Serializable]
	public partial class SceneReference {
#if UNITY_EDITOR
	// What we use in editor to select the scene
	[SerializeField] private UnityEngine.Object m_sceneAsset;
	
	private SceneAsset GetSceneAssetFromPath() {
		return string.IsNullOrEmpty(m_scenePath) ? null : AssetDatabase.LoadAssetAtPath<SceneAsset>(m_scenePath);
	}

	private string GetScenePathFromAsset() {
		return m_sceneAsset == null ? string.Empty : AssetDatabase.GetAssetPath(m_sceneAsset);
	}
#endif

		// This should only ever be set during serialization/deserialization in Editor.
		[SerializeField] private string m_scenePath = string.Empty;

		[SerializeField] private int m_buildIndex = -1;

		public string scenePath {
			get {
#if UNITY_EDITOR
			return GetScenePathFromAsset();
#else
				return m_scenePath;
#endif
			}
			set {
#if UNITY_EDITOR
			m_scenePath = value;
			m_sceneAsset = GetSceneAssetFromPath();
			m_buildIndex = SceneUtility.GetBuildIndexByScenePath(m_scenePath);
#else
				m_scenePath = value;
#endif
			}
		}

		public int buildIndex {
			get {
#if UNITY_EDITOR
			return SceneUtility.GetBuildIndexByScenePath(m_scenePath);
#else
				return m_buildIndex;
#endif
			}
		}

		public static implicit operator string(SceneReference sceneReference) {
			return sceneReference.scenePath;
		}
	}
}