namespace m21cerutti.BUS.Runtime {
	using UnityEngine;
	using UnityEngine.Events;

	[RequireComponent(typeof(Collider2D))]
	public class EventCollisionFilteredTrigger2D : MonoBehaviour {
		[SerializeField] private LayerMask m_layersDetected;

		[Header("Events"), SerializeField]
		
		
		private UnityEvent<GameObject> m_onTriggerFilteredEnter;

		[SerializeField] private UnityEvent<GameObject> m_onTriggerFilteredStay;

		[SerializeField] private UnityEvent<GameObject> m_onTriggerFilteredExit;

		public UnityEvent<GameObject> triggerFilteredEnter => m_onTriggerFilteredEnter;
		public UnityEvent<GameObject> triggerFilteredStay  => m_onTriggerFilteredStay;
		public UnityEvent<GameObject> triggerFilteredExit  => m_onTriggerFilteredExit;

		private void Start() {
			GetComponent<Collider2D>().isTrigger = true;
		}

		private void OnTriggerEnter2D(Collider2D other) {
			if (m_layersDetected.HaveLayer(other.gameObject.layer)) {
				m_onTriggerFilteredEnter.Invoke(other.gameObject);
			}
		}

		private void OnTriggerExit2D(Collider2D other) {
			if (m_layersDetected.HaveLayer(other.gameObject.layer)) {
				m_onTriggerFilteredExit.Invoke(other.gameObject);
			}
		}

		private void OnTriggerStay2D(Collider2D other) {
			if (m_layersDetected.HaveLayer(other.gameObject.layer)) {
				m_onTriggerFilteredStay.Invoke(other.gameObject);
			}
		}
	}
}