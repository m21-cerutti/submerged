namespace m21cerutti.BUS.Editor {
	using System.Collections.Generic;
	using System.Linq;

	using m21cerutti.ClapClapEvent.Runtime;

	using UnityEditor;
	using UnityEditor.PackageManager;
	using UnityEditor.PackageManager.Requests;

	using UnityEngine;

	[InitializeOnLoad]
	public class InstallPackagesOnLoad {
		private static PackageList m_List;
		private static List<AddRequest> m_Requests = new List<AddRequest>();

		static InstallPackagesOnLoad() {
			m_List = AssetDatabase.LoadAssetAtPath<PackageList>(
				AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("t:PackageList")[0]));
			InstallEnabledPackages();
		}

		[MenuItem("Tools/BaseUtilityScripts/Import packages")]
		public  static void InstallEnabledPackages() {
			foreach (GitPackage package in m_List.m_packages) {
				if (!package.m_enabled || UnityAssetUtilities.IsPackageInstalled(package.m_name)) {
					continue;
				}
				m_Requests.Add(Client.Add(package.m_url));
				EditorApplication.update += Progress;
			}
		}

		private static void Progress() {
			if (m_Requests.Count > 0) {
				foreach (AddRequest req in m_Requests.Where(req => req.IsCompleted)) {
					switch (req.Status) {
						case StatusCode.Success:
							Debug.Log("Installed: " + req.Result.packageId);
							break;
						case StatusCode.Failure:
							Debug.Log(req.Error.message);
							break;
					}
				}
			} 
			else {
				EditorApplication.update -= Progress;
			}
		}
	}
}