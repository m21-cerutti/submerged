﻿namespace m21cerutti.BUS.Editor {
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Text;

	using m21cerutti.ClapClapEvent.Runtime;

	using UnityEditor;

	using UnityEngine;

	public class GitPackagesEditor : EditorWindow {

		[SerializeField] private PackageList m_assetPackages;
		
		private Editor m_packagesEditor;
		private SerializedProperty m_propPackages;
		private Vector2 m_scrollPos;
		private SerializedObject m_soThis;
		
		private void OnEnable() {
			ScriptableObject target = this;
			m_soThis = new SerializedObject(target);
			m_assetPackages = AssetDatabase.LoadAssetAtPath<PackageList>(
						AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("t:PackageList")[0]));
			m_propPackages = m_soThis.FindProperty("m_assetPackages");
			m_packagesEditor = Editor.CreateEditor(m_assetPackages);
		}

		private void OnGUI() {
			EditorGUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
			m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);
			EditorGUILayout.Separator();
			GUILayout.Label("BUS Packages Manager", EditorStyles.whiteLargeLabel);
			EditorGUILayout.Separator();
			FuncEditor.DrawUILine(Color.gray);

			// Packages
			EditorGUILayout.PropertyField(m_propPackages, true);
			m_packagesEditor.DrawDefaultInspector();
			m_soThis.ApplyModifiedProperties();
			EditorGUILayout.Separator();

			if (GUILayout.Button("Import packages")) {
				InstallPackagesOnLoad.InstallEnabledPackages();
			}
			
			EditorGUILayout.Space(30);

			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndVertical();
		}

		[MenuItem("Tools/BaseUtilityScripts/Git packages editor", false, 0)]
		public static void ShowWindow() {
			EditorWindow win = GetWindow(typeof(GitPackagesEditor));
		}
	}
}