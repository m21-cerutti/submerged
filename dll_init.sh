#!/usr/bin/env bash

mkdir -p "Assets/Plugins/Kinect/"
mkdir -p "Build/"

export BODY_TRACKING_SDK_PATH="/c/Program Files/Azure Kinect Body Tracking SDK"
export BODY_TRACKING_TOOLS_PATH="$BODY_TRACKING_SDK_PATH/tools"
export BODY_TRACKING_LIB_PATH="$BODY_TRACKING_SDK_PATH/sdk/netstandard2.0/release"
export UNITY_PLUGINS_PATH="Assets/Plugins/Kinect"

cp "packages/System.Buffers.4.5.1/lib/netstandard2.0/System.Buffers.dll" $UNITY_PLUGINS_PATH
cp "packages/System.Memory.4.5.4/lib/netstandard2.0/System.Memory.dll" $UNITY_PLUGINS_PATH
cp "packages/System.Runtime.CompilerServices.Unsafe.6.0.0/lib/netstandard2.0/System.Runtime.CompilerServices.Unsafe.dll" $UNITY_PLUGINS_PATH
cp "packages/System.Reflection.Emit.Lightweight.4.7.0/lib/netstandard2.0/System.Reflection.Emit.Lightweight.dll" $UNITY_PLUGINS_PATH

cp "packages/Microsoft.Azure.Kinect.Sensor.1.4.1/lib/netstandard2.0/Microsoft.Azure.Kinect.Sensor.dll" $UNITY_PLUGINS_PATH
cp "packages/Microsoft.Azure.Kinect.Sensor.1.4.1/lib/netstandard2.0/Microsoft.Azure.Kinect.Sensor.pdb" $UNITY_PLUGINS_PATH
cp "packages/Microsoft.Azure.Kinect.Sensor.1.4.1/lib/netstandard2.0/Microsoft.Azure.Kinect.Sensor.deps.json" $UNITY_PLUGINS_PATH
cp "packages/Microsoft.Azure.Kinect.Sensor.1.4.1/lib/netstandard2.0/Microsoft.Azure.Kinect.Sensor.xml" $UNITY_PLUGINS_PATH
cp "packages/Microsoft.Azure.Kinect.Sensor.1.4.1/lib/native/amd64/release/depthengine_2_0.dll" $UNITY_PLUGINS_PATH
cp "packages/Microsoft.Azure.Kinect.Sensor.1.4.1/lib/native/amd64/release/k4a.dll" $UNITY_PLUGINS_PATH
cp "packages/Microsoft.Azure.Kinect.Sensor.1.4.1/lib/native/amd64/release/k4arecord.dll" $UNITY_PLUGINS_PATH

cp "$BODY_TRACKING_LIB_PATH/Microsoft.Azure.Kinect.BodyTracking.dll" $UNITY_PLUGINS_PATH
cp "$BODY_TRACKING_LIB_PATH/Microsoft.Azure.Kinect.BodyTracking.pdb" $UNITY_PLUGINS_PATH
cp "$BODY_TRACKING_LIB_PATH/Microsoft.Azure.Kinect.BodyTracking.deps.json" $UNITY_PLUGINS_PATH
cp "$BODY_TRACKING_LIB_PATH/Microsoft.Azure.Kinect.BodyTracking.xml" $UNITY_PLUGINS_PATH

cp "$BODY_TRACKING_TOOLS_PATH"/*.dll $UNITY_PLUGINS_PATH
cp "$BODY_TRACKING_TOOLS_PATH"/*.dll ./
cp "$BODY_TRACKING_TOOLS_PATH"/*.onnx $UNITY_PLUGINS_PATH
cp "$BODY_TRACKING_TOOLS_PATH"/*.onnx ./

./dll_build.sh

echo "Done dll init."